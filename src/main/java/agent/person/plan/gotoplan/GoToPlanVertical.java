package agent.person.plan.gotoplan;

import agent.person.GoToGoal;
import agent.person.plan.MoveExecutionError;

import java.awt.*;

/**
 * This subclass of the GoToPlan moves vertically before moving horizontally.
 */
public class GoToPlanVertical extends GoToPlan {
    public GoToPlanVertical(GoToGoal goal){
        super(goal);
    }

    @Override
    protected MoveExecutionError.MOVE_DIRECTION determineNextStep(Point location, Point destination) {
        MoveExecutionError.MOVE_DIRECTION direction = MoveExecutionError.MOVE_DIRECTION.INVALID;
        if (location.y > destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.NORTH;
        } else if (location.y < destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.SOUTH;
        } else if (location.x < destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.EAST;
        } else if(location.x > destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.WEST;
        }
        return direction;
    } 
}