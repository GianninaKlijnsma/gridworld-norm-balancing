package agent.person.plan.gotoplan;

import agent.person.GoToGoal;
import agent.person.plan.MoveExecutionError;

import java.awt.*;

/**
 * This subclass of the GoToPlan moves horizontally before moving vertically.
 */
public class GoToPlanHorizontal extends GoToPlan {
    public GoToPlanHorizontal(GoToGoal goal){
        super(goal);
    }

    @Override
    protected MoveExecutionError.MOVE_DIRECTION determineNextStep(Point location, Point destination) {
        MoveExecutionError.MOVE_DIRECTION direction = MoveExecutionError.MOVE_DIRECTION.INVALID;
        if(location.x > destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.WEST;       
        } else if (location.x < destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.EAST;
        } else if (location.y < destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.SOUTH;
        } else if (location.y > destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.NORTH;
        }
        return direction;
    } 

}