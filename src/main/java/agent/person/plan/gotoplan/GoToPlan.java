package agent.person.plan.gotoplan;

import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import norms.DefaultNorm;
import agent.person.GoToGoal;
import agent.person.plan.MoveExecutionError;
import agent.person.context.PersonContext;
import agent.person.context.PersonHistoryContext;
import agent.person.context.PersonSocialContext;

import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.agent.State;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.agent.NormContext;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;
import org.uu.nl.net2apl.core.plan.builtin.EvaluablePlan;

import java.util.List;
import java.util.ArrayList;
import java.awt.*;
import java.util.concurrent.TimeUnit;

/**
 * A plan to go to some specific coordinates on the grid. The plan is triggered
 * once by a {@code GoToGoal} and executed during each deliberation cycle
 * thereafter until the goal has been achieved. During each deliberation cycle
 * (i.e. each execution of this plan), the agent senses its position in the
 * environment, and tries to take a single step towards the goal. If the action
 * fails the path is likely blocked and a PlanExecutionError is thrown, which is
 * automatically adopted by the agent as an InternalTrigger, which triggers a
 * Repair plan (in this case {@code MoveRepairPlan}).
 *
 * This plan does not look ahead and just tries to move in the desired
 * direction. It is left as an exercise to the reader to make this plan more
 * intelligent, by taking into account possible paths and blockages, and by
 * planning ahead.
 */
public abstract class GoToPlan extends EvaluablePlan {

    public static int AFTER_MOVE_DELAY = 0;

    protected Point destination;
    private Point currentLocation;
    private Environment env;
    private AgentID agentID;
    private GoToGoal goal;
    private List<MoveExecutionError.MOVE_DIRECTION> actions;
    private ArrayList<Point> route;

    /**
     * Default constructor
     * 
     * @param goal The GoToGoal which triggered this plan, containing a destination
     */
    public GoToPlan(GoToGoal goal) {
        this.goal = goal;
        this.destination = goal.getDestination();
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        PersonContext context = planToAgentInterface.getContext(PersonContext.class);
        PersonHistoryContext historyContext = planToAgentInterface.getContext(PersonHistoryContext.class);

        this.currentLocation = context.getCurrentLocation();
        this.env = context.getEnvironment();
        this.agentID = planToAgentInterface.getAgentID();

        MoveExecutionError.MOVE_DIRECTION direction = this.actions.get(0);     

        try {            
            checkNormConformity(planToAgentInterface, direction);
            context.setCurrentLocation(this.env.sensePosition(planToAgentInterface.getAgentID()));
            attemptMove(direction);
            context.setCurrentLocation(this.env.sensePosition(planToAgentInterface.getAgentID()));
        } catch (ExternalActionFailedException e) {
            // System.err.format("[GoToPlan - %s] - Tried to move %s from (%dx%d) towards (%dx%d). Failed due to '%s'\n",
            // planToAgentInterface.getAgentID(), direction.toString(), context.getCurrentLocation().x, context.getCurrentLocation().y,
            // destination.x, destination.y, e.getMessage());
            // System.err.flush();

            goal.setPursued(false);
            this.route = new ArrayList<Point>();
            this.actions = new ArrayList<MoveExecutionError.MOVE_DIRECTION>();            
            context.resetCurrentRoute();

            // The {@code MoveExecutionError} extends the {@code PlanExecutionError}, meaning this goal 
            // will not be executed anymore until the MoveRepairPlan has been executed and signalled 
            // execution of this plan may continue
            throw new MoveExecutionError(this.goal, direction);
        }        

        // Add current state to agent's history
        historyContext.addState(planToAgentInterface.getBeliefState());
        // Every time an agent moves over the grid, their resource increments by 1
        context.setResourceValue(context.getResourceValue() + 1);
        // Remove the executed action from the list
        this.actions.remove(0);

        if (context.getCurrentLocation().equals(this.destination)) {
            // The plan is finished when the destination is reached. After which, it should not be executed again.
            if (this.goal != null)
                this.goal.markAchieved();
            setFinished(true);
            context.resetCurrentRoute();
        }
    }

    @Override
    public List<Object> getActionIDs(PlanToAgentInterface planToAgentInterface) {
        PersonContext context = planToAgentInterface.getContext(PersonContext.class);  
        currentLocation = context.getCurrentLocation();

        // Remove previous two routes from the context
        if(context.getCurrentRoutes().size() == 2){
            context.resetCurrentRoute();
        }

        planRoute(this.currentLocation);   
        context.addCurrentRoute(this.route);

        List<Object> actionList = new ArrayList<Object>();
        for(MoveExecutionError.MOVE_DIRECTION action : this.actions){
            actionList.add((Object)action);
        }        
        return actionList;
    }

    /**
     * Plans a route from the current agent's location to the goal
     * @param location The agent's current location
     */
    public void planRoute(Point location){
        this.actions = new ArrayList<MoveExecutionError.MOVE_DIRECTION>();
        this.route = new ArrayList<Point>();
        Point loc = new Point();
        loc.setLocation(location.x, location.y);

        // While we are not at the destination, find the next direction to move to
        while(!loc.equals(this.destination)){
            MoveExecutionError.MOVE_DIRECTION dir = determineNextStep(loc, this.destination);
            this.actions.add(dir);
            switch(dir){
                case EAST:
                    loc.setLocation(loc.getX() + 1, loc.getY());
                    break;
                case WEST:
                    loc.setLocation(loc.getX() - 1, loc.getY());
                    break;
                case NORTH:
                    loc.setLocation(loc.getX(), loc.getY() - 1);
                    break;
                case SOUTH:
                    loc.setLocation(loc.getX(), loc.getY() + 1);
                    break;
                case INVALID:
                    break;
            }
            Point newLoc = new Point();
            newLoc.setLocation(loc.x, loc.y);
            this.route.add(newLoc);
        }
    }
    
    /**
     * Pick one of four directions which will move the agent closer to its destination
     * @param location      The current location of the agent
     * @param destination   The location that the agent wants to move to
     * @return              One of four directions to move towards
     */
    protected abstract MoveExecutionError.MOVE_DIRECTION determineNextStep(Point location, Point destination);

    /**
     * Attempt to move one step in the chosen direction
     * @param direction     A wind direction to move towards
     * @throws ExternalActionFailedException    If the environment prohibits moving a step in the specified direction
     */
    private void attemptMove(MoveExecutionError.MOVE_DIRECTION direction) throws ExternalActionFailedException {
        delay();
        switch(direction) {
            case EAST:
                this.env.east(this.agentID);
                break;
            case WEST:
                this.env.west(this.agentID);
                break;
            case NORTH:
                this.env.north(this.agentID);
                break;
            case SOUTH:
                this.env.south(this.agentID);
                break;
        }
    }

    private void delay() {
        if(GoToPlan.AFTER_MOVE_DELAY > 0) {
            try {
                TimeUnit.MILLISECONDS.sleep(GoToPlan.AFTER_MOVE_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check if any norms are violated, and if so, apply sanctions
     * @param planToAgentInterface   
     * @param direction              The direction the agent is moving into
     */
    private void checkNormConformity(PlanToAgentInterface planToAgentInterface, MoveExecutionError.MOVE_DIRECTION direction) throws ExternalActionFailedException{  
        State state = planToAgentInterface.getBeliefState();  
        NormContext normContext = planToAgentInterface.getContext(NormContext.class);    
        PersonContext context = planToAgentInterface.getContext(PersonContext.class);    

        // If the agent has no norm base, return
        if(normContext == null) return;

        // Check for norm conformity
        for(Norm norm : normContext.getNorms()){                
            if(norm.isDetached(state)){
                boolean violated = norm.isViolated(direction);    
                
                // Update conformity rate of norm and inform other agents
                if(norm instanceof DefaultNorm){                    
                    inform("normConformity", ((DefaultNorm)norm).getNormID(), violated ? -1.0 : 1.0, planToAgentInterface);              

                    // Apply sanctions if norm is violated
                    if(violated){             
                        Double sanction = ((DefaultNorm)norm).getSanction();                
                        // If sanction makes resource invalid, throw MoveExecutionError
                        if(context.getResourceValue() - sanction < context.getCurrentResource().getMinValue()){   
                            throw new ExternalActionFailedException("Cannot violate norm, or resource will become invalid!");
                        }                                          
                        context.setResourceValue(context.getResourceValue() - sanction);
                    }
                }
            }
        }
    }

    /**
     * Send a message to an agent's acquaintances to inform them that a norm has been obeyed or violated
     * @param directive The string that informs the receiver that this message contains the conformity rate of a norm
     * @param normID The identifier of the norm
     * @param conformityRate The conformity rate of the norm
     * @param planToAgentInterface 
     */
    private void inform(String directive, String normID, Double conformityRate, PlanToAgentInterface planToAgentInterface) {
        PersonSocialContext socialContext = planToAgentInterface.getContext(PersonSocialContext.class);
        ACLMessage newMessage = new ACLMessage(Performative.INFORM);
        newMessage.setSender(planToAgentInterface.getAgentID());
        newMessage.setContent(directive + " " + normID + " " + conformityRate.toString());
        newMessage.setReceivers(socialContext.getAddressees());
        for (AgentID agentID : socialContext.getAddressees()) {
            planToAgentInterface.sendMessage(agentID, newMessage);
        } 
    }
}
