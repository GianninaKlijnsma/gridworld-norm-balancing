package agent.person.plan;

import agent.person.GoToGoal;
import agent.person.plan.gotoplan.GoToPlanVertical;
import agent.person.plan.gotoplan.GoToPlanHorizontal;

import org.uu.nl.net2apl.core.plan.builtin.EvaluablePlan;
import org.uu.nl.net2apl.core.plan.builtin.EvaluablePlanOptions;

import java.util.List;
import java.util.ArrayList;

/**
 * This class represents a set of plans that an agent can choose for execution to 
 * reach a single goal. In particular, this class gives two routes that the agent
 * can choose to reach its destination. The first plan prefers going horizontally 
 * before going vertically, and the other plan prefers going vertically before going
 * horizontally. Both of these plans are evaluated for execution, such that the best
 * plan is chosen. 
 */
public class GoToPlanOptions extends EvaluablePlanOptions {
    public static int AFTER_MOVE_DELAY = 0;
    private GoToGoal goal;

    public GoToPlanOptions(GoToGoal goal){
        this.goal = goal;
    }

    @Override
    public List<EvaluablePlan> getPlanOptions(){
        List<EvaluablePlan> options = new ArrayList<EvaluablePlan>();
        options.add(new GoToPlanVertical(this.goal));
        options.add(new GoToPlanHorizontal(this.goal));
        setEvaluated(true);
        return options;
    }
}