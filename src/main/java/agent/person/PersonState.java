package agent.person;

import agent.person.context.PersonContext;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.State;

import java.awt.*;

/**
 * This class represents the state that the "person" agent is in. The state
 * keeps track of the agent's location, and its resource value. The location
 * is needed by the Exploration property to check if a state has been visited
 * already, and is also used to check whether or not a norm is detached. The
 * resource value is needed for the calculation of the Resource property.
 */
public class PersonState implements State{
    private Point location;
    private Double money;

    public PersonState(){
        location = new Point();
        money = 0.0;
    }

    /**
     * @return The location in this state
     */
    public Point getLocation(){
        return (Point) this.location.clone();
    }

    /**
     * Set the location of this state
     * @param loc The location to set the state's location to
     */
    public void setLocation(Point loc){
        this.location = loc.getLocation();
    }
    
    /**
     * @return The resource value in this state
     */
    public Double getResourceValue(){
        return this.money;
    }

    /**
     * Set the resource value in this state
     * @param value The value that the resource should be set to
     */
    public void setResourceValue(double value){
        this.money = value;
    }

    @Override
    public void initialise(AgentContextInterface contextInterface){
        PersonContext personContext = contextInterface.getContext(PersonContext.class);
        this.location = personContext.getCurrentLocation();
        this.money = personContext.getResourceValue();  
    }

    @Override
    public PersonState copy(){
        PersonState personState = new PersonState();
        personState.setLocation(this.location.getLocation());
        personState.setResourceValue(this.money);
        return personState;
    }

    /**
     * Return true if this state is equal to the given state
     * @param s The state which we want to check for equality
     * @return True if the two states have the same location, False otherwise
     */
    public boolean equals(State s){
        if(s instanceof PersonState) {
            Point otherLocation = ((PersonState)s).getLocation();
            return (otherLocation.getX() == this.location.getX() && otherLocation.getY() == this.location.getY());
        }
        return false;
    }
}