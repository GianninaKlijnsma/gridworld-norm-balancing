package agent.person.context;

import agent.person.PersonState;

import org.uu.nl.net2apl.core.agent.Context;
import org.uu.nl.net2apl.core.agent.State;

import java.util.List;
import java.util.ArrayList;

/**
 * The PersonHistoryContext keeps track of the states that are visited by the agent during execution. 
 * Two states are considered to be equal to one another when their locations are the same.
 */
public class PersonHistoryContext implements Context{
    private List<State> visitedStates;

    public PersonHistoryContext(){
        visitedStates = new ArrayList<State>();
    }

    /**
     * Add a state to the list of visited states 
     * @param state The state that must be added
     */
    public void addState(State state){
        visitedStates.add(state);
    }

    /**
     * @return Get the states that are visited by this agent
     */
    public List<State> getStates(){
        return  visitedStates;
    }

    /**
     * Return true if the given state has been visited by the agent 
     * @param state The state that we have to check
     */
    public boolean isVisited(State state){
        for(State s : visitedStates){            
            if(s instanceof PersonState && ((PersonState)s).equals(state)){
                return true;
            }
        }
        return false;
    }
}