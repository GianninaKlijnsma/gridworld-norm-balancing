package agent.person.context;

import agent.person.plan.MoveExecutionError;
import agent.person.PersonState;

import org.uu.nl.net2apl.core.agent.ActionContext;
import org.uu.nl.net2apl.core.agent.State;

import java.awt.*;

/**
 * The PersonActionContext is the ActionContext of the generic Person agents, as descibed
 * in the PersonContext class. It specifies the action beliefs of the agents, or the beliefs
 * over the effects on actions on a state.  
 */
public class PersonActionContext implements ActionContext {

    @Override
    public State analyseAction(State state, Object actionID){
        if(actionID instanceof MoveExecutionError.MOVE_DIRECTION){
            return analyseMove(state, actionID);
        }
        return state;
    }

    /**
     * Analyse the moving in the grid     
     * @param state The state we will analyse the action in
     * @param actionID The identifier of the action
     * @return The state that the agent believes will be achieved after execution the action
     *         with action ID in the given state
     */
    public State analyseMove(State state, Object actionID){
        State newState = state.copy();
        if(actionID instanceof MoveExecutionError.MOVE_DIRECTION && state instanceof PersonState){
            MoveExecutionError.MOVE_DIRECTION direction = (MoveExecutionError.MOVE_DIRECTION)actionID;
            PersonState personState = (PersonState)newState;
            Point location = personState.getLocation();
            switch(direction){
                case EAST:
                    location.setLocation(location.getX() + 1, location.getY());
                    break;
                case WEST:
                    location.setLocation(location.getX() - 1, location.getY());
                    break;
                case NORTH:
                    location.setLocation(location.getX(), location.getY() - 1);
                    break;
                case SOUTH:
                    location.setLocation(location.getX(), location.getY() + 1);
                    break;
                case INVALID:
                    break;
            }            
            personState.setLocation(location);
            personState.setResourceValue(personState.getResourceValue() + 1);
            newState = personState;
        }
        return newState;
    }
    
}