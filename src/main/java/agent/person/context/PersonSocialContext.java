package agent.person.context;

import norms.DefaultNorm;

import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.Context;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The PersonSocialContext keeps track of an agent's acquiantances, and their last known conformity
 * rates for each of the norms in the agent's norm base. 
 */
public class PersonSocialContext implements Context {
    private List<AgentID> addressees;
    private HashMap<AgentID, HashMap<String, Double>> conformityRates;

    public PersonSocialContext(){
        addressees = new ArrayList<AgentID>();
        conformityRates = new HashMap<AgentID, HashMap<String, Double>>();
    }

    /**
     * Add a single addressee to the agent's social network
     * @param addressee The ID of the agent to be added
     */
    public void addAddressee(AgentID addressee){
        this.conformityRates.put(addressee, new HashMap<String, Double>());
        this.addressees.add(addressee);
    }

    /**
     * @return Returns the list of addressees in this agent's social network
     */
    public List<AgentID> getAddressees(){
        return this.addressees;
    }

    /**
     * Set the conformity rate of an addressee's norm to a given value
     * @param agentID The ID of the agent whose norm conformity must be updated
     * @param norm The norm whose conformity rate must be updated
     * @param value The value of the new conformity rate
     */
    public void setConformityRate(AgentID agentID, DefaultNorm norm, Double value){
        // If we haven't added this agent into the social network, add it
        if(!this.conformityRates.containsKey(agentID)) {
            addAddressee(agentID);
        }
        this.conformityRates.get(agentID).put(norm.getNormID(), value);
    }

    /**
     * Get an addressee's conformity rate of a given norm
     * @param agentID The ID of the addressee
     * @param norm The norm whose conformity rate must be returned
     * @return The addressee's conformity rate of the norm 
     */
    public Double getConformityRate(AgentID agentID, DefaultNorm norm){
        if(!this.conformityRates.get(agentID).containsKey(norm.getNormID())){
            this.conformityRates.get(agentID).put(norm.getNormID(), 0.0);
        }
        return this.conformityRates.get(agentID).get(norm.getNormID());
    }

    /**
     * A string list of this context, used in the GUI
     * @return A string list of this context
     */
    public List<String> getBeliefs(){
        List<String> beliefs = new ArrayList<String>();
        for(AgentID agentID : this.addressees){
            for(Map.Entry<String, Double> conformityBeliefs : this.conformityRates.get(agentID).entrySet()){
                beliefs.add(conformityBeliefs.getKey() + "(" + conformityBeliefs.getValue() + ")");
            }
        }
        return beliefs;
    }
}