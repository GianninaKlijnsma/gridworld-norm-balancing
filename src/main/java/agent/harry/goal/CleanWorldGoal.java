package agent.harry.goal;

import agent.harry.HarryContext;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Goal;

/**
 * Harries goal is to clean the world of bombs. This goal is pursued as long as there are bombs and bomb traps
 * remaining. If there are no bombs anymore, the plan is achieved. If there are no traps, it it unattainable. To stop
 * Harry from pursuing it, we mark this goal as achieved also in that case.
 */
public class CleanWorldGoal extends Goal {

    @Override
    public boolean isAchieved(AgentContextInterface agentContextInterface) {
        HarryContext c = agentContextInterface.getContext(HarryContext.class);
        return c.getBombLocations().isEmpty() && !c.isCarryingBomb() || c.getTrapLocations().isEmpty();
    }

    public String toString() {
        return "clean ( GridWorld )";
    }
}
