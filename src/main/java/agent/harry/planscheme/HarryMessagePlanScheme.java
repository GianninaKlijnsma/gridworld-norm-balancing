package agent.harry.planscheme;

import agent.harry.plan.HarryProcessMessagePlan;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

public class HarryMessagePlanScheme implements PlanScheme {

    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof ACLMessage) {
            ACLMessage t = (ACLMessage) trigger;
            if(t.getPerformative().equals(Performative.INFORM)) {
                plan = new HarryProcessMessagePlan(t);
            }
        }

        return plan;
    }
}
