package agent.harry.planscheme;

import agent.harry.HarryContext;
import agent.harry.goal.CleanWorldGoal;
import agent.harry.plan.*;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

/**
 * Harry's GoalPlanScheme matches a plan with an actively pursued goal. This plan scheme is triggered automatically
 * in each deliberation cycle if there is a goal in the agent's goal base.
 *
 * Harry's goal plan scheme triggers plans to make Harry move towards a bomb, pick up the bomb, move towards a trap
 * position, and then drop the trap, depending on the current state of Harry's beliefs.
 */
public class HarryGoalPlanScheme implements PlanScheme {
    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        // If no plan can be matched with the given trigger and the current state of affairs, an uninstantiated
        // plan is returned, which tells the agent to do nothing for this trigger
        Plan plan = Plan.UNINSTANTIATED;

        // The only Goal that triggers plans for Harry is the CleanWorldGoal
        if(trigger instanceof CleanWorldGoal) {
            // Get a reference for Harry's belief base
            HarryContext context = agentContextInterface.getContext(HarryContext.class);

            // If Harry is already carrying a bomb, a plan to get rid of it should be executed.
            if(context.isCarryingBomb()) {
                if(context.trapAtPosition()) {
                    // Harry beliefs there is a trap at his position, so a plan to drop the bomb is started
                    plan = new DropBombPlan();
                } else if (!context.getTrapLocations().isEmpty()) {
                    // Harry knows some other trap locations. A plan to go to the closest is started
                    plan = new GoToTrapPlan();
                } else {
                    // Harry needs to get rid of a bomb, but does not know any traps. Maybe Sally knows a few.
                    // Starting a plan to ask her
                    plan = new RequestInformationUpdatePlan();
                }
            } else if (!context.getBombLocations().isEmpty()) {
                if(context.bombAtPosition())
                    // Harry is not carrying a bomb, but there is a bomb at his position
                    plan = new PickupPlan();
                else
                    // Harry starts a plan to go to the nearest known bomb from his current position
                    plan = new GoToBombPlan();
            } else {
                // This is never triggered, since the goal is already achieved. Should sally be more clever,
                // or should Harry?
                plan = new RequestInformationUpdatePlan();
            }
        }

        try {
            plan.setPlanGoal(trigger);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // After the plan has been instantiated, it is returned to the deliberation cycle for execution
        return plan;
    }

}
