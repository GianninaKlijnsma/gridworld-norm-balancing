package agent.harry.plan;

import agent.harry.HarryContext;
import agent.person.GoToGoal;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * Very simple plan to go to the nearest trap position
 */
public class GoToTrapPlan extends Plan {

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        HarryContext context = planToAgentInterface.getContext(HarryContext.class);
        Point closestTrap = context.findClosestTrap();

        // Some sanity checks: Don't go to a bomb trap if not a bomb, and don't try to set a destination goal if you
        // already committed to another destination: Both goals will be pursued in the same deliberation round, meaning
        // neither goal will ever be achieved.
        if(context.isCarryingBomb()) {
            GoToGoal.adoptIfNoOther(new GoToGoal(closestTrap), planToAgentInterface);
        }

        // Always mark a plan as finished if it should not be repeated in the next deliberation cycle
        setFinished(true);
    }
}
