package agent.harry.plan;

import agent.harry.HarryContext;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

public class PickupPlan extends Plan {

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        HarryContext context = planToAgentInterface.getContext(HarryContext.class);

        // Sense the current position, in case it has run out of sync with beliefs
        try {
            Point position = context.getEnvironment().sensePosition(planToAgentInterface.getAgentID());
            context.setCurrentLocation(position);
        } catch(ExternalActionFailedException e) {
            System.err.println(e.getMessage());
        }

        boolean isBombRemoved = false;

        // Try to remove the bomb
        try {
            context.getEnvironment().pickup(planToAgentInterface.getAgentID());
            context.setCarryingBomb(true);
            isBombRemoved = true;
        } catch (ExternalActionFailedException e) {
            // Environment doesn't agree. Most likely scenario is that the bomb is no longer here.
            if(!context.isCarryingBomb()) isBombRemoved = true;
            System.err.println(e.getMessage());
        }

        if(isBombRemoved) {
            // Remove this bomb from the belief base, and tell Sally Harry no longer beliefs there to be a bomb here
            context.getBombLocations().remove(context.getCurrentLocation());
            planToAgentInterface.adoptPlan(
                    new InformNoObjectPlan(InformNoObjectPlan.ObjectType.BOMB, context.getCurrentLocation()));
            informRemoved(planToAgentInterface, context);
        }

        setFinished(true); // Make sure this plan can be readopted in the future
    }

    /**
     * Inform Sally there is no (longer) a bomb at the given position
     * @param planToAgentInterface  Plan to agent interface
     * @param context               Reference to Harry's belief base
     */
    private void informRemoved(PlanToAgentInterface planToAgentInterface, HarryContext context) {
        ACLMessage message = new ACLMessage(Performative.DISCONFIRM);
        message.setSender(planToAgentInterface.getAgentID());
        message.setReceivers(context.getInformingAgents());
        message.setContent(String.format("not bomb(%dx%d)", context.getCurrentLocation().x, context.getCurrentLocation().y));
        for(AgentID agentID : context.getInformingAgents()) {
            planToAgentInterface.sendMessage(agentID, message);
        }
    }
}
