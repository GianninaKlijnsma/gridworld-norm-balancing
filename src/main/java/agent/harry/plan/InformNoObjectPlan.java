package agent.harry.plan;

import agent.harry.HarryContext;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * A plan to inform scouting agents that a certain belief of an object at a certain position is (no longer) true.
 */
public class InformNoObjectPlan extends Plan {

    enum ObjectType {
        BOMB, TRAP, WALL;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    private ObjectType type;
    private Point point;

    public InformNoObjectPlan(ObjectType type, Point point) {
        this.type = type;
        this.point = point;
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        HarryContext context = planToAgentInterface.getContext(HarryContext.class);

        if(!context.getInformingAgents().isEmpty()) {
            ACLMessage message = new ACLMessage(Performative.DISCONFIRM);
            message.setContent(String.format("not %s(%d,%d)", this.type, this.point.x, this.point.y));
            message.setReceivers(context.getInformingAgents());

            for(AgentID agentID : context.getInformingAgents()) {
                planToAgentInterface.sendMessage(agentID, message);
            }
        }

        setFinished(true);
    }
}
