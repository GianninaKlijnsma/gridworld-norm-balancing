package agent.harry.plan;

import agent.ProcessMessagePlan;
import agent.harry.HarryContext;
import agent.harry.goal.CleanWorldGoal;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;
import java.util.regex.Pattern;

/**
 * Process incoming messages from Sally, which inform Harry of bomb or bomb trap locations
 */
public class HarryProcessMessagePlan extends ProcessMessagePlan {

    private static final Pattern bombPattern = Pattern.compile("bombAt" + encodedCoordinatePattern);
    private static final Pattern trapPattern = Pattern.compile("trapAt" + encodedCoordinatePattern);

    protected HarryContext context;
    private PlanToAgentInterface planToAgentInterface;

    public HarryProcessMessagePlan(ACLMessage message) {
        super(message);
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        this.context = planToAgentInterface.getContext(HarryContext.class);
        this.planToAgentInterface = planToAgentInterface;
        this.context.addInformingAgent(this.message.getSender());

        handleBombPattern();
        handleTrapPattern();
        updateNormConformity(planToAgentInterface);        

        setFinished(true);
    }

    private void handleBombPattern() {
        Point p = getCoordinates(this.message.getContent(), HarryProcessMessagePlan.bombPattern);
        if(p != null) {
            if(this.context.getBombLocations().add(p) && this.context.getBombLocations().size() == 1) {
                this.planToAgentInterface.adoptGoal(new CleanWorldGoal());
            }

            // If there is a bomb here, there can't be a trap. The line below removes the belief that there is a trap
            // in this location, if that belief existed.
            this.context.getTrapLocations().remove(p);
        }
    }

    private void handleTrapPattern() {
        Point p = getCoordinates(this.message.getContent(), HarryProcessMessagePlan.trapPattern);
        if(p != null) {
            if(this.context.getTrapLocations().add(p) && this.context.getTrapLocations().size() == 1) {
                this.planToAgentInterface.adoptGoal(new CleanWorldGoal());
            }

            // If there is a trap here, there can't be a bomb. The line below removes the belief that there is a bomb
            // in this location, if that belief existed
            this.context.getBombLocations().remove(p);
        }
    }
}
