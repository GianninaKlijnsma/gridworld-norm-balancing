package agent.harry.plan;

import agent.harry.HarryContext;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

/**
 * A plan to send messages to Sally (and to all agents who have informed Harry of positions of bombs or bomb traps)
 */
public class RequestInformationUpdatePlan extends Plan {

    private PlanToAgentInterface planToAgentInterface;
    private HarryContext context;

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        this.context = planToAgentInterface.getContext(HarryContext.class);

        if(this.context.getBombLocations().isEmpty())
            sendMessage("bombs");

        if(this.context.getTrapLocations().isEmpty())
            sendMessage("traps");

        setFinished(true);
    }

    private void sendMessage(String content) {
        if(!context.getInformingAgents().isEmpty()) {

            ACLMessage message = new ACLMessage(Performative.REQUEST);
            message.setSender(planToAgentInterface.getAgentID());
            message.setContent(content);
            message.setReceivers(context.getInformingAgents());

            for(AgentID agentID : context.getInformingAgents()) {
                planToAgentInterface.sendMessage(agentID, message);
            }
        }
    }
}
