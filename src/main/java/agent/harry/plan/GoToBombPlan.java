package agent.harry.plan;

import agent.harry.HarryContext;
import agent.person.GoToGoal;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * A simple plan to find the nearest bomb, and adopt a goal to go to that bomb
 */
public class GoToBombPlan extends Plan {
    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        HarryContext context = planToAgentInterface.getContext(HarryContext.class);

        try {
            context.setCurrentLocation(context.getEnvironment().sensePosition(planToAgentInterface.getAgentID()));
        } catch (ExternalActionFailedException e) {
            e.printStackTrace();
        }

        // Some sanity checks: Don't go to a new bomb if already carrying one (you can't carry more than one at a time),
        // and don't try to set a destination goal if you already committed to another destination: Both goals will be
        // pursued in the same deliberation round, meaning neither goal will ever be achieved.
        if(!context.isCarryingBomb() && context.getBombLocations().size() > 0) {
            Point closestBomb = context.findClosestBomb();

            // Create and adopt a goal to go to the selected bomb
            GoToGoal goToGoal = new GoToGoal(closestBomb);
            GoToGoal.adoptIfNoOther(goToGoal, planToAgentInterface);
        }

        // Always mark a plan as finished if it should not be repeated in the next deliberation cycle
        setFinished(true);
    }
}
