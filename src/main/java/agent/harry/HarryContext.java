package agent.harry;

import agent.person.context.PersonContext;
import environment.Environment;
import org.uu.nl.net2apl.core.agent.AgentID;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Harry's belief base, which can be queried by Harry for its beliefs, and updated when beliefs change.
 * It consists of a set of locations for bombs, walls, and traps, as well as a (relatively static) list
 * of Sally's in the environment.
 *
 * Since contexts, and thus belief bases, are Java classes, they can be extended. This context extends the
 * {@code PersonContext}, which keeps track of beliefs required to navigate the GridWorld environment.
 */
public class HarryContext extends PersonContext {

    /**
     * Other agents in the environment, which have sent Harry messages with locations for bombs, bomb traps, or walls.
     * These agents should be informed on the removal of one of these objects.
     */
    private final Set<AgentID> informingAgents = new HashSet<>();

    /**
     * Positions of various objects in the environment
     */
    private final Set<Point> bombLocations = new HashSet<>();
    private final Set<Point> wallLocations = new HashSet<>();
    private final Set<Point> trapLocations = new HashSet<>();

    /**
     * Harry keeps track of whether it is carrying a bomb
     */
    private boolean carryingBomb = false;

    /**
     * Default constructor for Harry's belief base
     * @param environment   Reference to the Grid World environment
     */
    public HarryContext(Environment environment) {
        super(environment);
    }

    /**
     * Get a list of trap locations of which Harry is aware
     *
     * @return  Harry's beliefs for trap positions in the environment
     */
    public synchronized Set<Point> getTrapLocations() {
        synchronized (trapLocations) {
            return trapLocations;
        }
    }

    /**
     * Get a list of bomb locations of which Harry is aware
     *
     * @return  Harry's beliefs for bomb positions in the environment
     */
    public synchronized Set<Point> getBombLocations() {
        synchronized (bombLocations) {
            return bombLocations;
        }
    }

    /**
     * Getter for whether Harry thinks he is carrying a bomb currently
     * @return  Boolean if Harry beliefs to be carrying a bomb
     */
    public boolean isCarryingBomb() {
        return carryingBomb;
    }

    /**
     * Method to update Harry's beliefs with regards to whether he is carrying a bomb
     * @param carryingBomb  Boolean, true indicating Harry's beliefs should be updated so he is carrying a bomb
     */
    public void setCarryingBomb(boolean carryingBomb) {
        this.carryingBomb = carryingBomb;
    }

    /**
     * Get the closest bomb in the belief base to Harry's current position
     * @return  The location of the closest known bomb
     */
    public synchronized Point findClosestBomb() {
        synchronized (this.bombLocations) {
            return closestItem(this.bombLocations);
        }
    }

    /**
     * Get the closest trap in the belief base to Harry's current position
     * @return  The location of the closest known bomb trap
     */
    public synchronized Point findClosestTrap() {
        synchronized (this.trapLocations) {
            return closestItem(this.trapLocations);
        }
    }

    /**
     * Check whether Harry beliefs there is a bomb at his current position
     * @return  True iff Harry beliefs his current position holds a bomb
     */
    public synchronized boolean bombAtPosition() {
        synchronized (this.bombLocations) {
            return itemAtPosition(this.bombLocations);
        }
    }

    /**
     * Check whether Harry beliefs there is a bomb trap at his current position
     * @return  True iff Harry beliefs his current position holds a bomb trap
     */
    public synchronized boolean trapAtPosition() {
        synchronized (this.trapLocations) {
            return itemAtPosition(this.trapLocations);
        }
    }

    /**
     * Add an agent that sent Harry a message to the belief base of Harry concerning agents that should
     * be updated when an object is removed from or no longer present in the environment.
     * @param agentID   AgentID of agent to send messages to
     */
    public synchronized void addInformingAgent(AgentID agentID) {
        synchronized (this.informingAgents) {
            this.informingAgents.add(agentID);
        }
    }

    /**
     * Remove an agent that sent Harry a message to the belief base of Harry concerning agents that should
     * be updated when an object is removed from or no longer present in the environment.
     * @param agentID   AgentID of agent to no longer send messages to
     */
    public synchronized void removeInformingAgent(AgentID agentID) {
        synchronized (this.informingAgents) {
            this.informingAgents.remove(agentID);
        }
    }

    /**
     * Get a list of agents Harry beliefs should be informed when objects are removed from or no longer present in
     * the environment
     *
     * @return Set of (2APL) agent ID's
     */
    public synchronized Set<AgentID> getInformingAgents() {
        synchronized (this.informingAgents) {
            return this.informingAgents;
        }
    }

    /**
     * A string list of Harry's belief base, used in the GUI
     * @return A string list of Harry's belief base
     */
    @Override
    public ArrayList<String> getBeliefs() {
        ArrayList<String> beliefs = super.getBeliefs();
        synchronized (this.bombLocations) {
            if (this.bombLocations.isEmpty()) beliefs.add("not bombs()");
            beliefs.addAll(this.bombLocations.stream().map(x -> String.format("bomb(%d,%d)", x.x, x.y)).collect(Collectors.toList()));
        }
        synchronized (this.trapLocations) {
            if (this.trapLocations.isEmpty()) beliefs.add("not traps()");
            beliefs.addAll(this.trapLocations.stream().map(x -> String.format("trap(%d,%d)", x.x, x.y)).collect(Collectors.toList()));
        }
        synchronized (this.wallLocations) {
            if (this.wallLocations.isEmpty()) beliefs.add("not walls()");
            beliefs.addAll(this.wallLocations.stream().map(x -> String.format("wall(%d,%d)", x.x, x.y)).collect(Collectors.toList()));
        }
        return beliefs;
    }
}
