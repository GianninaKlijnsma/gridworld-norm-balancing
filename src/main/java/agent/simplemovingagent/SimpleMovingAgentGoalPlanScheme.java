package agent.simplemovingagent;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

public class SimpleMovingAgentGoalPlanScheme implements PlanScheme {
    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof SimpleMovingGoal) {
            plan = new SimpleMovingPlan((SimpleMovingGoal) trigger);
        }

        if(trigger != null && plan != Plan.UNINSTANTIATED)
            plan.setPlanGoal(trigger);

        return plan;
    }
}
