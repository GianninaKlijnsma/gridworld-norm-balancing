package agent.simplemovingagent;

import agent.person.GoToGoal;
import agent.person.context.PersonContext;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * A simple plan to visit one or more cells in a grid. Triggered by the {@code SimpleMovingGoal}
 */
public class SimpleMovingPlan extends Plan {

    private SimpleMovingGoal goal;

    public SimpleMovingPlan(SimpleMovingGoal goal) {
        this.goal = goal;
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        PersonContext movingContext = planToAgentInterface.getContext(PersonContext.class);

        if(movingContext.getCurrentLocation().equals(this.goal.getCurrentPoint())) {
            this.goal.setCurrentVisisted();

            if(this.goal.hasNext()) {
                Point nextPoint = this.goal.getCurrentPoint();
                GoToGoal moveGoal = new GoToGoal(nextPoint);
                System.out.println("Adopting goal to go to " + nextPoint);
                planToAgentInterface.adoptGoal(moveGoal);
            } else {
                setFinished(true);
            }
        }
    }
}
