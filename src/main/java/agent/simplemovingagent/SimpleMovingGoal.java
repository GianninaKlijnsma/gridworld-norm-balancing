package agent.simplemovingagent;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Goal;

import java.awt.*;
import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * The SimpleMovingGoal (associated with the {@code SimpleMovingPlan}) can be used to test how well an agent is able
 * to move towards a goal destination on the grid. The goal makes the agent move to each square in the grid once.
 */
public class SimpleMovingGoal extends Goal {
    private ArrayList<PointBooleanTuple> points = new ArrayList<>();
    private int index;

    public SimpleMovingGoal(Point... points) {
        this.index = 0;
        addPoint(points);
    }

    public void setCurrentVisisted() {
        this.points.get(index).isVisisted();
        index++;
    }

    public void addPoint(Point... points) {
        for(Point p : points) {
            this.points.add(new PointBooleanTuple(p));
        }
    }

    public boolean hasNext() {
        return this.index < this.points.size();
    }

    public Point getCurrentPoint() {
;        Point result = this.points.get(this.index).getPoint();
        return result;
    }

    @Override
    public boolean isAchieved(AgentContextInterface agentContextInterface) {
        return this.points.stream().map(PointBooleanTuple::isVisisted).allMatch(Predicate.isEqual(Boolean.TRUE));
    }

    private class PointBooleanTuple {
        private Point p;
        private boolean isVisisted = false;

        public PointBooleanTuple(Point p) {
            this.p = p;
        }

        protected void setVisisted() {
            this.isVisisted = true;
        }

        protected boolean isVisisted() {
            return this.isVisisted;
        }

        protected Point getPoint() {
            return this.p;
        }
    }
}
