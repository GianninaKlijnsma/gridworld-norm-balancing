package agent.sally.goal;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Goal;

/**
 * A goal for Sally to achieve is to search the Grid World for new bombs and bomb traps. This goal can never actually
 * be achieved, because Sally should continue searching in case new bombs or bomb traps appear.
 */
public class SearchGridWorldGoal extends Goal {
    @Override
    public boolean isAchieved(AgentContextInterface agentContextInterface) {
        return false;
    }

    public String toString() {
        return "search( GridWorld )";
    }
}
