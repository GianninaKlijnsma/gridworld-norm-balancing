package agent.sally.plan;

import agent.ProcessMessagePlan;
import agent.sally.SallyContext;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;
import java.util.regex.Pattern;

/**
 * A plan to process messages from other agents
 */
public class SallyProcessMessagePlan extends ProcessMessagePlan {

    private static final Pattern noBombPattern = Pattern.compile("not bomb" + encodedCoordinatePattern);
    private static final Pattern noTrapPattern = Pattern.compile("not trap" + encodedCoordinatePattern);

    private SallyContext context;

    public SallyProcessMessagePlan(ACLMessage message) {
        super(message);
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        this.context = planToAgentInterface.getContext(SallyContext.class);

        if(this.message.getPerformative().equals(Performative.REQUEST)) {
            if(this.message.getContent().equalsIgnoreCase("traps"))
                // Harry informed Sally it does not have any trap locations
                this.context.clearTraps();
            else if (this.message.getContent().equalsIgnoreCase("bombs"))
                // Harry informed Sally it does not know any other bomb locations
                this.context.clearBombs();
        } else {
            handleNoBombPattern();
            handleNoTrapPattern();
            updateNormConformity(planToAgentInterface);
        }

        setFinished(true);
    }

    /**
     * If the message indicates the other agent no longer beliefs there to be a bomb somewhere, remove this bomb
     * position from the belief base
     */
    private void handleNoBombPattern() {
        Point p = getCoordinates(this.message.getContent(), SallyProcessMessagePlan.noBombPattern);
        if(p != null) {
            this.context.setNoBomb(p);
        }
    }

    /**
     * If the message indicates the other agent no longer beliefs there to be a bomb trap somewhere, remove this bomb
     * trap position from the belief base
     */
    private void handleNoTrapPattern() {
        Point p = getCoordinates(this.message.getContent(), SallyProcessMessagePlan.noTrapPattern);
        if(p != null) {
            this.context.setNoTrap(p);
        }
    }
}
