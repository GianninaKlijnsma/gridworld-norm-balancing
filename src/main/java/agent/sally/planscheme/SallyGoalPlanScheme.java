package agent.sally.planscheme;

import agent.sally.goal.SearchGridWorldGoal;
import agent.sally.plan.SearchGridWorldPlan;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

/**
 * The Goal Plan Scheme matches a plan to a goal. In this case, the only processed goal is the {@code SearchGridkWorldGoal},
 * which triggers the {@SearchGridWorldPlan}. The goal is associated with the plan using the {@code setPlanGoal} method,
 * which tells the agent not to trigger a new instance of that plan in deliberation cycles to come, as long as the first
 * instance is not explicitly marked as finished. This allows the agent to plan ahead once, and execute one or more steps
 * of that plan during each deliberation cycle, until the plan either is finished or has failed.
 */
public class SallyGoalPlanScheme implements PlanScheme {
    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof SearchGridWorldGoal) {
            plan = new SearchGridWorldPlan();
        }

        if(trigger != null && plan != Plan.UNINSTANTIATED)
            plan.setPlanGoal(trigger);

        return plan;
    }
}
