package agent;

/**
 * This class represents a resource that the agents can use. Initially, the agents
 * start with a value of 10, and they gain +1 resource each time they move. 
 */
public class Resource {
    double value = 0;

    public Resource(){
        this.value = 10;
    }

    /**
     * @return The name of this resource.
     */
    public String getName(){
        return "money";
    }

    /**
     * Set the value of the resource to the given value
     * @param val The value that the resource should be set to
     */
    public void setValue(double val){
        val = Math.max(val, getMinValue());
        val = Math.min(val, getMaxValue());
        this.value = val;
    }

    /**     
     * @return The value of this resource
     */
    public double getValue(){
        return this.value;
    }
     
    /**
     * @return The maximum value that the resource can have
     */
    public double getMaxValue(){
        return 100;
    }

    /**
     * @return The minimum value that the resource can have
     */
    public double getMinValue(){
        return 0;
    }
}