package agent;

import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.agent.NormContext;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.plan.Plan;

import agent.person.context.PersonSocialContext;
import norms.DefaultNorm;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Since both Harry and Sally can receive messages in which coordinates are encoded as a string,
 * this abstract class handles the parsing of coordinates from a string message. Both Harry and Sally
 * extend this plan to extract the information from messages to their specific needs.
 */
public abstract class ProcessMessagePlan extends Plan {

    protected static final String encodedCoordinatePattern = "\\(\\s*(\\d+)\\s*[,;]\\s*(\\d+)\\s*\\)";
    protected static final Pattern testPattern = Pattern.compile("\\b(normConformity)\\b\\s(\\w+)\\s(.*)");
    protected static final Pattern normConformityPattern = Pattern.compile("\\b(normConformity)\\b\\s(\\w+)\\s(\\d*\\.?\\d*)");

    protected ACLMessage message;

    public ProcessMessagePlan(ACLMessage message) {
        this.message = message;
    }

    protected Point getCoordinates(String message, Pattern pattern) {
        Matcher matcher = pattern.matcher(message);
        if(matcher.matches()) {
            return new Point(
                Integer.parseInt(matcher.group(1)),
                Integer.parseInt(matcher.group(2)));
        } else {
            return null;
        }
    }

    /**
     * Update the agents context over an addressee's norm conformity that it received
     * via a message from that addressee.
     * @param planToAgentInterface
     */
    public void updateNormConformity(PlanToAgentInterface planToAgentInterface){
        PersonSocialContext socialContext = planToAgentInterface.getContext(PersonSocialContext.class);
        NormContext normContext = planToAgentInterface.getContext(NormContext.class);
        String content = this.message.getContent();     

        Matcher matcher = testPattern.matcher(content);
        if(matcher.matches()){
            AgentID agentID = this.message.getSender();
            String normID = matcher.group(2);     
            double conformityRate = Double.parseDouble(matcher.group(3));

            for(Norm n : normContext.getNorms()){                
                if(n instanceof DefaultNorm && ((DefaultNorm)n).getNormID().equals(normID)){
                    socialContext.setConformityRate(agentID, (DefaultNorm)n, conformityRate);
                    return;
                }
            }
        }   
    }
}
