package properties;

import org.uu.nl.net2apl.core.properties.Property;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.State;
import org.uu.nl.net2apl.core.agent.Norm;

import java.util.List;

/**
 * This property gives a value to how 'restricted' a plan is for an agent. We say
 * a state is restricted if at least one norm is detached in it. A value of +1 
 * specifies that all states in the plan are not restricted by a norm, and a value 
 * of -1 specifies that all states are restricted. Values in between specify a 
 * mixture between these.  
 */
public class FreedomProperty implements Property {
    @Override
    public String getName(){
        return "Freedom";
    }

    @Override
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface){
        List<State> states = propertyToPlanInterface.getStates();
        List<Norm> norms = propertyToPlanInterface.getDetachedNorms();

        // Get the ratio of 'restricted' states in the plan (states where a norm is detached)
        int nrOfRestrictedStates = 0;
        for(State s : states){
            for(Norm n : norms){
                if(n.isDetached(s)){
                    nrOfRestrictedStates--;
                    break;
                }
            }
            nrOfRestrictedStates++;
        }
        double freedom = nrOfRestrictedStates / (double)states.size();
        return freedom;
    }
}