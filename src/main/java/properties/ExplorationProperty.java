package properties;

import agent.person.context.PersonHistoryContext;

import org.uu.nl.net2apl.core.properties.Property;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.State;

import java.util.List;

/**
 * This property gives a value to how explorative a plan is for an agent.
 * A value of +1 specifies that all states in the plan have not been visited yet,
 * and a value of -1 specifies that all states have already been visited. Values
 * in between specify a mixture between these.  
 */
public class ExplorationProperty implements Property {
    @Override
    public String getName(){
        return "Exploration";
    }

    @Override
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface){
        List<State> states = propertyToPlanInterface.getStates();
        PersonHistoryContext historyContext = contextInterface.getContext(PersonHistoryContext.class);

        // Find the ratio of unvisited states in the plan
        int nrOfExplorativeStates = 0;
        for(State state : states.subList(1, states.size())){
            if(!historyContext.isVisited(state)){
                nrOfExplorativeStates += 1;
            }
            else{
                nrOfExplorativeStates -= 1;
            }
        }
        double exploration = nrOfExplorativeStates / ((double)states.size() - 1);
        return exploration;
    }
}