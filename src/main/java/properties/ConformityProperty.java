package properties;

import org.uu.nl.net2apl.core.properties.Property;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;

/**
 * This property gives a value to the ratio of obeyed norms versus violated norms.
 * A value of +1 specifies that all norms detached in the plan are obeyed, and a
 * value of -1 specifies that all norms are violated. Values in between specify a 
 * mixture between these.  
 */
public class ConformityProperty implements Property {
    @Override
    public String getName(){
        return "Conformity";
    }

    @Override
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface){
        int nrOfObeyedNorms = propertyToPlanInterface.getObeyedNorms().size();
        int nrOfViolatedNorms = propertyToPlanInterface.getViolatedNorms().size();
        int totalNorms = nrOfObeyedNorms + nrOfViolatedNorms;
        
        // Get the conformity rate of the plan, on a scale of -1 to +1
        double conformity = 0;
        if(totalNorms > 0) {
            conformity = (nrOfObeyedNorms - nrOfViolatedNorms) / (double)(totalNorms);
        }
        else{
            // If there are no norms, simply put the conformity rate at 0
            conformity = 0;
        }
        return conformity;
    }
}