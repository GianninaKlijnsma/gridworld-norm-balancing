package properties;

import agent.Resource;
import agent.person.PersonState;
import agent.person.context.PersonContext;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.properties.Property;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;

import java.util.List;

/**
 * This property gives a value to how 'resourceful' a plan is for an agent. A value 
 * of +1 specifies that we gained so much of a resource in a plan that we cannot have
 * any more, and a value of -1 specifies that we have lost as much as we can of a 
 * resource. Values in between specify a mixture between these.  
 */
public class ResourceProperty implements Property{
    @Override
    public String getName(){
        return "Resource";
    }

    @Override
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface){
        // Get the resource that the agent currently has, and the value of the resource in the last state of analysation   
        Resource resource = contextInterface.getContext(PersonContext.class).getCurrentResource();
        double finalValue = ((PersonState)propertyToPlanInterface.getLastState()).getResourceValue();

        // Apply sanctions from violated norms
        finalValue = applySanctions(propertyToPlanInterface.getViolatedNorms(), finalValue);
        
        // Calculate the amount of resource gained or lost during the plan analysing
        double resourceSum = finalValue - resource.getValue();

        // Find the corresponding denominator of the plan
        double denominator = 0;
        if(resourceSum < 0 && resource.getValue() != resource.getMinValue()){
            denominator = resource.getValue() - resource.getMinValue();
        }
        else if(resourceSum > 0 && resource.getValue() != resource.getMaxValue()) {
            denominator = resource.getMaxValue() - resource.getValue();
        }
        else{
            denominator = 1.0;
        }

        // If money resource is invalid, return -1
        if(resource.getValue() + resourceSum < resource.getMinValue()){
            return -1.0;
        }
        else if(resource.getValue() + resourceSum > resource.getMaxValue()){
            return 1.0;
        }

        double value = resourceSum / (double)denominator;
        return value;     
    }

    public double applySanctions(List<Norm> violatedNorms, double value){
        // Apply sanctions
        for(Norm norm : violatedNorms){
            Object sanction = norm.getSanction();
            if(sanction instanceof Double){
                value -= (Double)sanction;
            }            
        }
        return value;
    }
}