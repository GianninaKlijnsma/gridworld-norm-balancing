package properties;

import agent.person.context.PersonSocialContext;
import norms.DefaultNorm;

import org.uu.nl.net2apl.core.properties.Property;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Norm;

import java.util.List;

/**
 * This property gives a value to how pressured an agent is to obey or violate
 * the detached norms in a plan, given the confomity rates of these norms for its
 * acquiantances. A value of +1 means that all the norms in the plan have been obeyed
 * or violated in such a way that it conforms with that the agent believes its 
 * acquiantances are doing. A value of -1 specifies the opposite. Values in between 
 * specify a mixture between these.  
 */
public class SocialPressureProperty implements Property {
    @Override
    public String getName(){
        return "SocialPressure";
    }

    @Override
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface){
        List<Norm> obeyedNorms = propertyToPlanInterface.getObeyedNorms();
        List<Norm> violatedNorms = propertyToPlanInterface.getViolatedNorms();
        PersonSocialContext socialContext = contextInterface.getContext(PersonSocialContext.class);

        double desire = 0;
        for(Norm norm : obeyedNorms){
            desire += calculateDesire(socialContext, (DefaultNorm)norm, true);
        }
        for(Norm norm : violatedNorms){
            desire += calculateDesire(socialContext, (DefaultNorm)norm, false);
        }

        int totalNorms = propertyToPlanInterface.getDetachedNorms().size();
        if(totalNorms > 0) {
            desire = desire / (double)(totalNorms);
        }
        return desire;
    }

    /**
     * Calculates the desire of the norm's obeyance based on the addressees' conformity (or violation) rate
     * of the norm, and the trust of the addressee. 
     * @param socialContext The context that holds information on the addressees and their conformity rates
     * @param norm The norm to be evaluated
     * @param obeyed A boolean indication whether the norm was obeyed or not
     * @return A value indicating the desire of violationg or obeying that norm
     */
    private double calculateDesire(PersonSocialContext socialContext, DefaultNorm norm, boolean obeyed){
        double desire = 0;
        for(AgentID agentID : socialContext.getAddressees()){
            if(obeyed) {
                desire += socialContext.getConformityRate(agentID, norm);
            }
            else{
                desire += -1 * socialContext.getConformityRate(agentID, norm);
            }
        }
        return desire;
    }
}