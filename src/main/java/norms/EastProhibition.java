package norms;

import agent.person.plan.MoveExecutionError.MOVE_DIRECTION;

import java.awt.*;

/**
 * Specifies a prohibition to move east at a given location.
 */
public class EastProhibition extends DirectionProhibition {
    public EastProhibition(Point location){
        super(location);
    }

    @Override
    public Object getActionID(){
        return MOVE_DIRECTION.EAST;
    }

    @Override
    public String getNormID(){
        return "eastProhibition";
    }
}