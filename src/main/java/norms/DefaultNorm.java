package norms;

import org.uu.nl.net2apl.core.agent.Norm;

/**
 * An extension of the norm interface, that implements a new method
 * for the programmer to override, namely the ID of the norm. Furthermore,
 * the getSanction() function returns a double instead of an object, 
 * because norms in our example affect the value of a resource.
 */
public interface DefaultNorm extends Norm {
    /**
     * @return The name of the norm
     */
    public String getNormID();

    /**
     * @return The sanction, specified in terms of a resource
     */
    public Double getSanction();
}