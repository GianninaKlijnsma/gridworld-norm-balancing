package norms;

import agent.person.plan.MoveExecutionError.MOVE_DIRECTION;

import java.awt.*;

/**
 * Specifies a prohibition to move south at a given location.
 */
public class SouthProhibition extends DirectionProhibition {
    public SouthProhibition(Point location){
        super(location);
    }

    @Override
    public Object getActionID(){
        return MOVE_DIRECTION.SOUTH;
    }

    @Override
    public String getNormID(){
        return "southProhibition";
    }
}