package norms;

import agent.person.plan.MoveExecutionError.MOVE_DIRECTION;

import java.awt.*;

/**
 * Specifies a prohibition to move west at a given location.
 */
public class WestProhibition extends DirectionProhibition {
    public WestProhibition(Point location){
        super(location);
    }

    @Override
    public Object getActionID(){
        return MOVE_DIRECTION.WEST;
    }

    @Override
    public String getNormID(){
        return "westProhibition";
    }
}