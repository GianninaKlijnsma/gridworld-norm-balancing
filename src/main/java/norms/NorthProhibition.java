package norms;

import agent.person.plan.MoveExecutionError.MOVE_DIRECTION;

import java.awt.*;

/**
 * Specifies a prohibition to move north at a given location.
 */
public class NorthProhibition extends DirectionProhibition {
    public NorthProhibition(Point location){
        super(location);
    }

    @Override
    public Object getActionID(){
        return MOVE_DIRECTION.NORTH;
    }

    @Override
    public String getNormID(){
        return "northProhibition";
    }
}