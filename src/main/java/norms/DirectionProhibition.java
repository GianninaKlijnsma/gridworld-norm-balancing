package norms;

import org.uu.nl.net2apl.core.agent.State;
import agent.person.PersonState;

import java.awt.*;

/**
 * A prohibition that prohibits an agent from moving into a certain direction,
 * if they are at a certain location. For example, the agent can be prohibited
 * to move south if they are at location (2,3). 
 */
public abstract class DirectionProhibition implements DefaultNorm {
    Point detachedLocation;

    public DirectionProhibition(Point detachedLocation){
        this.detachedLocation = detachedLocation;
    }

    /**
     * @return The identifier of the direction is prohibited
     */
    protected abstract Object getActionID();

    /**
     * @return The detached location of this norm. 
     */
    public Point getLocation(){
        return detachedLocation;
    }

    @Override
    public Double getSanction(){
        return 10.0;
    }

    @Override
    public boolean isDetached(State state){
        if(state instanceof PersonState){
            PersonState personState = (PersonState)state;
            Point loc = personState.getLocation();
            return loc.getX() == detachedLocation.getX() && loc.getY() == detachedLocation.getY();
        }
        return false;
    }

    @Override
    public boolean isViolated(Object actionID){
        return actionID == getActionID();    
    }
}