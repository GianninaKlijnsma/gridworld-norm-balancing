import agent.person.GoToGoal;
import agent.person.PersonState;
import agent.person.context.PersonContext;
import agent.person.context.PersonHistoryContext;
import agent.person.context.PersonSocialContext;
import environment.AgentInterface;

import org.jetbrains.annotations.Nullable;
import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.agent.State;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.agent.NormContext;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class APLToEnvAgent implements AgentInterface {

    private final Agent agent;
    private String humanName;

    public APLToEnvAgent(Agent agent) {
        this.agent = agent;
        this.humanName = agent.getAID().getShortLocalName();
    }

    public APLToEnvAgent(Agent agent, String humanName) {
        this.agent = agent;
        this.humanName = humanName;
    }

    /**
     * The agent interface should generate an identifier that will be used by both the agents to identify
     * them with the environment, as by the environment to signal events to an agent
     *
     * @return An object used as an identifier
     */
    @Override
    public Object getAgentIdentifier() {
        return this.agent.getAID();
    }

    /**
     * A human-readable name for the agent
     *
     * @return String
     */
    @Override
    public @Nullable String getAgentName() {
        return this.humanName;
    }

    /**
     * A list of active goals, in Predicate-logic notation (e.g. "goto(X,Y)"). Used to show belief base of this agent in
     * the GUI
     *
     * @return List of active goals of the agent
     */
    @Override
    public List<String> getPursuingGoals() {
        try {
            synchronized (this.agent) {
                return this.agent.getGoals().stream().map(Goal::toString).collect(Collectors.toList());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    /**
     * A list of beliefs in Predicate-logic notation (e.g. "bomb(X,Y)"). Used to show belief base of this agent in
     * the GUI
     *
     * @return List of beliefs of the agent
     */
    @Override
    public List<String> getBeliefs() {
        synchronized (this.agent) {
            List<String> personBeliefs = this.agent.getContext(PersonContext.class).getBeliefs();
            personBeliefs.add(String.format("location(%dx%d)", 
                    this.agent.getContext(PersonContext.class).getCurrentLocation().x,
                    this.agent.getContext(PersonContext.class).getCurrentLocation().y));
            personBeliefs.add("money(" + this.agent.getContext(PersonContext.class).getResourceValue() + ")");        
            personBeliefs.addAll(this.agent.getContext(PersonSocialContext.class).getBeliefs());            
            return personBeliefs;
        }
    }

    /**
     * Returns the cell coordinates in the GridWorld that the agent is currently (trying to) move towards
     *
     * @return Point on GridWorld
     */
    @Override
    public @Nullable Point getCurrentTargetDestination() {
        synchronized (this.agent) {
            var goals = this.agent.getGoals();
            for(Goal g : goals) {
                if(g instanceof GoToGoal) {
                    return ((GoToGoal) g).getDestination();
                }
            }
        }
        return null;
    }

    /**
     * Get a list of GridWorld coordinates representing the planned path the agent will take towards its
     * destination
     *
     * @return List of adjoining GridWorld coordinates
     */
    @Override
    public @Nullable ArrayList<Point> getCurrentPlannedPath() {
        synchronized(this.agent){
            ArrayList<ArrayList<Point>> route = this.agent.getContext(PersonContext.class).getCurrentRoutes();
            if(route == null){
                return new ArrayList<>();
            }
            ArrayList<Point> result = new ArrayList<Point>();
            route.forEach(result::addAll);
            return result;
        }
    }

    /**
     * Get the agent's norms from its norm base
     * 
     * @return List of norms from the agent's norm base
     */
    @Override
    public @Nullable List<Norm> getNorms(){
        NormContext normContext = agent.getContext(NormContext.class);
        if(normContext != null){
            return normContext.getNorms();
        }
        else return new ArrayList<Norm>();
    }

    /**
     * Get the list of positions that are already visited by the agent
     * 
     * @return The list of positions visited by the agent
     */
    @Override
    public @Nullable List<Point> getVisitedLocations(){
        List<State> visitedStates = agent.getContext(PersonHistoryContext.class).getStates(); 
        List<Point> visitedPoints = new ArrayList<Point>();
        for(State s : visitedStates){
            if(s instanceof PersonState){
                visitedPoints.add(((PersonState)s).getLocation());
            }
        }
        return visitedPoints;
    }
}
