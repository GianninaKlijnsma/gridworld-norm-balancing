import agent.harry.HarryContext;
import agent.harry.planscheme.HarryGoalPlanScheme;
import agent.harry.planscheme.HarryMessagePlanScheme;
import agent.person.plan.gotoplan.GoToPlan;
import agent.person.context.PersonActionContext;
import agent.person.context.PersonContext;
import agent.person.planscheme.PersonGoalPlanScheme;
import agent.person.planscheme.PersonInternalTriggerPlanScheme;
import agent.person.PersonState;
import agent.person.context.PersonHistoryContext;
import agent.person.context.PersonSocialContext;
import agent.sally.SallyContext;
import agent.sally.goal.SearchGridWorldGoal;
import agent.sally.planscheme.SallyGoalPlanScheme;
import agent.sally.planscheme.SallyMessagePlanScheme;
import agent.simplemovingagent.SimpleMovingAgentGoalPlanScheme;
import agent.simplemovingagent.SimpleMovingGoal;
import norms.EastProhibition;
import norms.NorthProhibition;
import norms.SouthProhibition;
import norms.WestProhibition;
import properties.ConformityProperty;
import properties.FreedomProperty;
import properties.ExplorationProperty;
import properties.ResourceProperty;
import properties.SocialPressureProperty;
import environment.AgentInterface;
import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import environment.view.AgentColor;

import org.uu.nl.net2apl.core.agent.ActionContext;
import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.AgentArguments;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.fipa.FIPAMessenger;
import org.uu.nl.net2apl.core.platform.Platform;
import org.uu.nl.net2apl.core.agent.NormContext;

import java.awt.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * The main class instantiates a Grid World environment and populates it with BDI agents.
 * The Grid World environment is a two dimensional grid, on which bombs, walls, and bomb traps can occur.
 * The agents are tasked with finding bombs and bringing them to bomb traps. Agents have a certain sense radius within
 * the grid, can only pickup a bomb if they are in the same location as the bomb and aren't currently carrying a bomb,
 * can only drop a bomb if they are in the same location as a bomb trap and are carrying a bomb and can't move through
 * walls or other agents.
 *
 * In this demo, two types of agents exist: Harry and Sally. Harry does not sense anything, but only brings bombs to
 * bomb traps. Sally does not carry any bombs, but constantly scouts the world for bombs and bomb traps, informing Harry
 * of every discovery.
 */
public class Main {

    // Starting points for Harry and Sally Agents
    private static final Point HARRY_START_POSITION = new Point(0, 0);
    private static final Point SALLY_START_POSITION = new Point(8, 8);

    // Weights for Harry
    private static final Double EXPLORATION_WEIGHT_HARRY = 0.0;
    private static final Double CONFORMITY_WEIGHT_HARRY = 0.0;
    private static final Double FREEDOM_WEIGHT_HARRY = 0.0;
    private static final Double RESOURCE_WEIGHT_HARRY = 0.0;
    private static final Double SOCIALPRESSURE_WEIGHT_HARRY = 0.0;

    // Weights for Sally
    private static final Double EXPLORATION_WEIGHT_SALLY = 0.0;
    private static final Double CONFORMITY_WEIGHT_SALLY = 0.0;
    private static final Double FREEDOM_WEIGHT_SALLY = 0.0;
    private static final Double RESOURCE_WEIGHT_SALLY = 0.0;
    private static final Double SOCIALPRESSURE_WEIGHT_SALLY = 0.0;

    // The set of norms
    private static ArrayList<Norm> norms;

    // Agents may move a bit fast. To show them moving over the grid, delay for this number of milliseconds (0 means
    // no delay)
    public static final int AFTER_MOVE_DELAY = 200;

    private static int nHarries = 1;
    private static int nSallies = 1;

    private static final int ENV_WIDTH = 15;
    private static final int ENV_HEIGHT = 15;

    // For simulation purposes, a headless environment can be instantiated, meaning no view is created
    private static final boolean INSTANTIATE_ENVIRONMENT_HEADLESS = false;

    private static Platform platform;
    private static Environment environment;

    public static ArrayList<AgentInterface> agents = new ArrayList<>();

    public static void main(String[] args) {
        setup();
        initNorms();
        initiateWithHarryAndSally();

        // #Uncomment next line to test walking agent
        // addSimpleMovingAgent(platform, environment);
    }

    /**
     * Creates an environment and 2APL platform
     */
    private static void setup() {
        FIPAMessenger messenger = new FIPAMessenger();

        Main.platform = Platform.newPlatform(4, messenger);

        Main.environment = new Environment(
                new Dimension(Main.ENV_WIDTH, Main.ENV_HEIGHT),
                INSTANTIATE_ENVIRONMENT_HEADLESS);

        GoToPlan.AFTER_MOVE_DELAY = Main.AFTER_MOVE_DELAY;
    }

    /**
     * Creates the Harry and Sally agents, based on set parameters, and puts them on the 2APL platform and places them
     * in the environment
     */
    private static void initiateWithHarryAndSally() {
        for(int i = 0; i < Main.nHarries; i++) {
            Point startingPosition = Main.nHarries <= 1 ?
                    Main.HARRY_START_POSITION : Main.environment.getFirstFree(Main.HARRY_START_POSITION);

            if(startingPosition == null) {
                System.err.format("No more free cells in environment. Can't add %dth Harry agent.\n", i);
                return;
            }

            // Harry does not need to be instantiated with a CleanWorldGoal, as the goal is adopted automatically
            // after the first message from Sally
            Main.createAgent(
                    getHarryArguments(startingPosition),
                    startingPosition,
                    AgentColor.BLUE,
                    Main.nHarries > 1 ? String.format("Harry-%d", i) : "Harry"
            );
        }

        for(int i = 0; i < Main.nSallies; i++) {
            Point startingPosition = Main.nSallies <= 1 ?
                    Main.SALLY_START_POSITION : Main.environment.getFirstFree(Main.SALLY_START_POSITION);
            if(startingPosition == null) {
                System.err.format("No more free cells in environment. Can't add %dth Sally agent.\n", i);
                return;
            }
            Main.createAgent(
                    getSallyArguments(startingPosition),
                    startingPosition,
                    AgentColor.RED,
                    Main.nSallies > 1 ? String.format("Sally-%d", i) : "Sally",
                    new SearchGridWorldGoal()
            );
        }
    }

    /**
     * A simple moving agent can be used to demonstrate how agents may move through the environment without doing
     * anything else
     */
    private static void initiateWithSimpleMovingAgent() {
        Main.createAgent(
                getSimpleMovingAgentArguments(new Point(0,0)),
                new Point(0,0),
                AgentColor.ORANGE,
                "SimpleMoving",
                getSimpleMovingGoal()
        );
    }

    /**
     * A goal is used to activate a SimpleMovingAgent. This particular goal makes the SimpleMovingAgent move over
     * each cell in the grid iteratively
     *
     * @return  SimpleMovingGoal    A goal for the agent to move to every cell of the grid iteratively
     */
    private static SimpleMovingGoal getSimpleMovingGoal() {
        Point[] pointsToVisit = new Point[Main.ENV_WIDTH * Main.ENV_HEIGHT];
        int index = 0;
        for(int i = 0; i < Main.ENV_WIDTH; i++) {
            for(int j = 0; j < Main.ENV_HEIGHT; j++) {
                pointsToVisit[index] = new Point(i, j);
                index++;
            }
        }

        return new SimpleMovingGoal(pointsToVisit);
    }

    /**
     * Create a 2APL
     *
     * @param arguments             AgentArguments to use to instantiate a new agent
     * @param startingPosition      The coordinates the agent spawned at in the environment (must be empty)
     * @param color                 The color with which this agent will show up in the interface
     * @param localShortName        A short identifier used for logging
     * @param initialGoals          An initial set of goals for the agent to pursue (may be empty)
     * @return                      Agent instantiated with the passed parameters
     */
    private static boolean createAgent(AgentArguments arguments, Point startingPosition, AgentColor color, String localShortName, Goal... initialGoals) {
        arguments.addGoalPlanScheme(new PersonGoalPlanScheme());
        arguments.addInternalTriggerPlanScheme(new PersonInternalTriggerPlanScheme());

        Agent agent;
        AgentID agentID;
        try {
            agentID = new AgentID(localShortName, UUID.randomUUID(), platform.getHost(), platform.getPort());            
        } catch (URISyntaxException e){
            e.printStackTrace();
            return false;
        }

        agent = new Agent(platform, arguments, agentID);
        for(Goal g : initialGoals) {
            agent.adoptGoal(g);
        }
        return enter(agent,startingPosition, color, localShortName);
    }

    /**
     * Create an interfaced agent which can be added to the grid world environment. Add the agent to the environment
     * @param agent             2APL agent object to add to the environment
     * @param start             Start position where the agent should enter the environment
     * @param color             Agent display color in the GUI
     * @param shortLocalName    A short local name used to identify this agent in the GUI
     */
    private static boolean enter(Agent agent, Point start, AgentColor color, String shortLocalName) {
        APLToEnvAgent interfacedAgent = new APLToEnvAgent(agent, shortLocalName);
        Main.agents.add(interfacedAgent);

        try {
            environment.enter(interfacedAgent, start, color);
            return true;
        } catch(ExternalActionFailedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * An agent is instantiated using Agent Arguments. The arguments allow adding plans, beliefs, goals and plan schemes
     * to the agent. A plan scheme uses beliefs to instantiate a plan based on an active goal during each deliberation
     * cycle. Beliefs are added in the form of Contexts, which are generic Java objects on which all relevant beliefs
     * can be encoded as class members
     *
     * Harry's belief base consists of locations of bombs, bomb traps, and walls of which it is informed by Sally, plus
     * a belief for its current location. It has plans to find closest bombs or closest traps, or to pick up or drop a
     * bomb. The belief base triggers the right plan dependent on the current situation of Harry (e.g. where is it,
     * is it carrying any bombs).
     *
     * A message plan scheme is added to initiate a plan when Harry receives a message from Sally. This plan processes
     * the message (which contains locations for bombs or bomb traps).
     *
     * @param startingPosition  Harry's starting position, which will be set as a belief for Harry
     *
     * @return Agent Arguments with which Harry can be instantiated.
     */
    private static AgentArguments getHarryArguments(Point startingPosition) {
        AgentArguments arguments = new AgentArguments();
        HarryContext context = new HarryContext(Main.environment);
        context.setCurrentLocation(startingPosition);
        arguments.addContext(context, HarryContext.class, PersonContext.class);
        arguments.setBeliefState(new PersonState());
        arguments = addContexts(arguments);
        arguments.addGoalPlanScheme(new HarryGoalPlanScheme());
        arguments.addMessagePlanScheme(new HarryMessagePlanScheme());

        arguments = initPropertyBase(arguments);
        arguments = initHarryWeights(arguments);

        return arguments;
    }

    /**
     * Sally's belief base consists of locations of bombs, bomb traps and walls of which she has already informed Harry.
     * Every time she sends a message to Harry, these beliefs are updated. Beliefs are removed when Harry informs her
     * he picked up a bomb, or when Harry informs her there is no longer a bomb at that position.
     *
     * Sally has plans to roam the Grid World randomly, continuously scouting the environment for bombs, bomb traps, and
     * walls. Every time she encounters one of those she sends a message to Harry with its position. Sally has a constant
     * maintainance goal (i.e. never achieved but constantly pursued) to continue scouting the Grid World, which triggers
     * the previously described plan using the plan schemes.
     *
     * The message plan scheme is added to initiate a plan when Sally receives a message from Harry, informing her of
     * the removal of a bomb or a trap from the environment. This plan scheme triggers a plan to process that message
     * and update the belief base accordingly.
     *
     * @param startingPosition  Sally's starting position, which will be set as a belief for Sally
     *
     * @return Agent Arguments with which Sally can be instantiated.
     */
    private static AgentArguments getSallyArguments(Point startingPosition) {
        AgentArguments arguments = new AgentArguments();
        SallyContext context = new SallyContext(Main.environment);
        context.setCurrentLocation(startingPosition);
        Main.agents.forEach(x -> context.addAgent((AgentID) x.getAgentIdentifier()));
        arguments.addContext(context, SallyContext.class, PersonContext.class);
        arguments.setBeliefState(new PersonState());
        arguments = addContexts(arguments);
        arguments.addGoalPlanScheme(new SallyGoalPlanScheme());
        arguments.addMessagePlanScheme(new SallyMessagePlanScheme());
        
        arguments = initPropertyBase(arguments);
        arguments = initSallyWeights(arguments);

        return arguments;
    }

    /**
     * Agent arguments containing a simple plan and plan scheme to visit a certain set of cells in the Grid World
     *
     * @param startingPosition The starting position of this agent
     * @return                 Agent arguments with which a SimpleMovingAgent can be instantiated
     */
    private static AgentArguments getSimpleMovingAgentArguments(Point startingPosition) {
        AgentArguments args = new AgentArguments();
        args.addGoalPlanScheme(new SimpleMovingAgentGoalPlanScheme());
        args.addContext(new PersonContext(Main.environment, new Point(0,0)), PersonContext.class);
        args.setBeliefState(new PersonState());
        args = addContexts(args);
        args = initPropertyBase(args);
        args = initSallyWeights(args);
        return args;
    }

    /**
     * Adds a number of contexts to the agent arguments
     * @param arguments The agent arguments to which the contexts must be added to
     * @return The agent arguments with the contexts added
     */
    private static AgentArguments addContexts(AgentArguments arguments){
        // Add history and action context
        arguments.addContext(new PersonHistoryContext(), PersonHistoryContext.class);
        arguments.addContext(new PersonActionContext(), PersonActionContext.class, ActionContext.class);

        // Add Norm context
        NormContext normContext = new NormContext();
        Main.norms.forEach(x -> normContext.addNorm(x));
        arguments.addContext(normContext, NormContext.class);
        
        // Add social context
        PersonSocialContext socialContext = new PersonSocialContext();
        Main.agents.forEach(x -> socialContext.addAddressee((AgentID) x.getAgentIdentifier()));
        arguments.addContext(socialContext, PersonSocialContext.class);
        return arguments;
    }

    /**
     * Initialises the property base
     * @param arguments The agent arguments to which the properties must be added to
     * @return The agent arguments with the properties added
     */
    private static AgentArguments initPropertyBase(AgentArguments agentArgs){
        agentArgs.addProperty(new ExplorationProperty());
        agentArgs.addProperty(new ConformityProperty());
        agentArgs.addProperty(new FreedomProperty());
        agentArgs.addProperty(new ResourceProperty());
        agentArgs.addProperty(new SocialPressureProperty());
        return agentArgs;
    } 

    /**
     * Initialises weights for each property for Sally. Sally will prefer and prefers conforming to norms.
     * However, if Harry violated enough norms, Sally might start violating them as well due to the social pressure.
     * @param arguments The agent arguments to which the weights must be added to
     * @return The agent arguments with the weights added
     */
    private static AgentArguments initSallyWeights(AgentArguments agentArgs){
        agentArgs.addWeight("Exploration", EXPLORATION_WEIGHT_SALLY);
        agentArgs.addWeight("Conformity", CONFORMITY_WEIGHT_SALLY);
        agentArgs.addWeight("Freedom", FREEDOM_WEIGHT_SALLY);
        agentArgs.addWeight("Resource", RESOURCE_WEIGHT_SALLY);
        agentArgs.addWeight("SocialPressure", SOCIALPRESSURE_WEIGHT_SALLY);
        return agentArgs;
    }

    /**
     * Initialises weights for each property for Harry. Harry will prefer plans that do nor conforming to norms, 
     * and will prefer doing the opposite to Sally. 
     * @param arguments The agent arguments to which the weights must be added to
     * @return The agent arguments with the weights added
     */
    private static AgentArguments initHarryWeights(AgentArguments agentArgs){
        agentArgs.addWeight("Exploration", EXPLORATION_WEIGHT_HARRY);
        agentArgs.addWeight("Conformity", CONFORMITY_WEIGHT_HARRY);
        agentArgs.addWeight("Freedom", FREEDOM_WEIGHT_HARRY);
        agentArgs.addWeight("Resource", RESOURCE_WEIGHT_HARRY);
        agentArgs.addWeight("SocialPressure", SOCIALPRESSURE_WEIGHT_HARRY);
        return agentArgs;
    }

    /**
     * Initialises the norms that both agents will add to their norm base upon initialisation.
     * These include the norms that prohibit an agent to move west, east, north or south at
     * specific locations
     */
    private static void initNorms(){
        // Init the locations where one can't turn north, east, or west
        norms = new ArrayList<Norm>();

        for(int y = 5; y <= 10; y++){
            Point eastLoc = new Point();
            eastLoc.setLocation(3, y);
            norms.add(new EastProhibition(eastLoc));

            Point westLoc = new Point();
            westLoc.setLocation(11, y);            
            norms.add(new WestProhibition(westLoc));
        }
        
        for(int x = 4; x <= 10; x++){
            Point southLoc = new Point();
            southLoc.setLocation(x, 4);
            norms.add(new SouthProhibition(southLoc));

            Point northLoc = new Point();
            northLoc.setLocation(x, 11);
            norms.add(new NorthProhibition(northLoc));
        }
    }
}
