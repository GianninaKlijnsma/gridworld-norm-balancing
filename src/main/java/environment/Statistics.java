package environment;

import environment.objects.AgentObject;

import javax.swing.table.AbstractTableModel;
import java.util.*;

public class Statistics extends AbstractTableModel implements Observer {

    public Statistics(Environment env, EnvironmentView view) {
        this.setEnvironmentView(view);
        this.environment = env;
    }

    private EnvironmentView view;
    private Environment environment;

    private int rowCount = 0;
    private static final int COLUMN_COUNT = 2;

    private BeliefGoalObject[] goalsnBeliefs;

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(int column) {
        return column == 0 ? "State" : "Agent";
    }

    @Override
    public Object getValueAt(int row, int column) {
        for(int i = 0; i < this.goalsnBeliefs.length; i++) {
            if(row < this.goalsnBeliefs[i].getRowCount()) {
                return this.goalsnBeliefs[i].getValueAt(row, column);
            } else {
                row -= this.goalsnBeliefs[i].getRowCount();
            }
        }

        return null;
    }

    private void setEnvironmentView(EnvironmentView environmentView) {
        if(this.view != null) this.view.removeChangeListener(this);
        this.view = environmentView;
        this.view.addChangeListener(this);

    }

    @Override
    public void update(Observable observable, Object o) {
        update();
    }

    public void update() {
        List<AgentObject> agents = view.getSelectedAgent() == null ?
                new LinkedList<>(this.environment.getGridWorldAgents()) :
                Collections.singletonList(view.getSelectedAgent());
        this.goalsnBeliefs = new BeliefGoalObject[agents.size()];
        this.rowCount = 0;


        if(agents.size() > 0) {
            for(int i = 0; i < agents.size(); i++) {
                this.goalsnBeliefs[i] = new BeliefGoalObject(agents.get(i).getAgent());
                this.rowCount += this.goalsnBeliefs[i].getRowCount();
            }
        }

        this.rowCount += 1;

        // notify observers
        fireTableRowsUpdated(0, this.rowCount);

        // Changed SA: This is better, this will not change the layout of the complete table!
        fireTableDataChanged();
    }

    private class BeliefGoalObject {
        private AgentInterface agent;
        private final List<String> beliefs;
        private final List<String> goals;

        public BeliefGoalObject(AgentInterface agent) {
            this.agent = agent;
            this.beliefs = agent.getBeliefs();
            this.goals = agent.getPursuingGoals();
        }

        public int getRowCount() {
            synchronized (this.beliefs) {
                synchronized (this.goals) {
                    return this.beliefs.size() + this.goals.size();
                }
            }
        }

        public String getValueAt(int row, int column) {
            if(column == 1 && row == 0) return this.agent.getAgentName();
            else if (column == 0 && row < this.goals.size()) return String.format("G <%s>", this.goals.get(row));
            else if (column == 0 && row < this.beliefs.size()) return String.format("B <%s>", this.beliefs.get(row));

            return "";
        }
    }
}
