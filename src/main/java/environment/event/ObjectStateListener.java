package environment.event;

import environment.objects.PositionedObject;

public interface ObjectStateListener<T extends PositionedObject> {

    void added(T positionedObject);

    void removed(T positionedObject);

}
