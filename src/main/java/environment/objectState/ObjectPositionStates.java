package environment.objectState;

import environment.event.ObjectStateListener;
import environment.objects.PositionedObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Encodes a list of objects, each of which occupies exactly one cell in the grid
 * @param <T>   Type of object this list contains
 */
public class ObjectPositionStates<T extends PositionedObject> extends ArrayList<T> {

    private transient List<ObjectStateListener<T>> listeners = new ArrayList<>();

    @Override
    public boolean add(T t) {
        if(super.add(t)) {
            notifyObjectAdded(t);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void add(int index, T element) {
        super.add(index, element);
        notifyObjectAdded(element);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        if(super.addAll(c)) {
            c.forEach(this::notifyObjectAdded);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if(super.addAll(index, c)) {
            c.forEach(this::notifyObjectAdded);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Is there a T at this location
     * @param point     Point encoding X and Y coordinates
     * @return          True iff cell in grid at (X,Y) is occupied
     */
    public boolean isFree(Point point) {
        synchronized (this) {
            for (T t : this) {
                if (t.isPosition(point)) return false;
            }
        }
        return true;
    }

    /**
     * Find the object at coordinate (X,Y)
     * @param point     Point encoding X and Y coordinates
     * @return          T at (X,Y) if there is such an object, null otherwise
     */
    public T getObjectAt(Point point) {
        synchronized (this) {
            for (T next : this) {
                if (next.isPosition(point)) return next;
            }
        }
        return null;
    }

    /**
     * Remove an object at coordinate (X,Y)
     * @param point     Point encoding X and Y coordinates
     * @return          Removed object if there was an object at (X,Y), null otherwise
     */
    public T removeObjectAt(Point point) {
        synchronized (this) {
            for (T next : this) {
                if(next.isPosition(point)) {
                    this.remove(next);
                    return next;
                }
            }
        }
        return null;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
    }

    public boolean remove(T o) {
        if(super.remove(o)) {
            notifyObjectRemoved(o);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return c.stream().map(this::remove).allMatch(Predicate.isEqual(true));
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        // Not implemented
        return false;
    }

    @Override
    public T remove(int index) {
        T removed = super.remove(index);
        notifyObjectRemoved(removed);
        return removed;
    }

    /**
     * Clear all objects in this list with coordinates falling outside the X and Y coordinates
     * @param maxX      Maximum X coordinate
     * @param maxY      Maximum Y coordinate
     */
    public void removeAllOutOfBounds(int maxX, int maxY) {
        this.removeAllOutOfBounds(0,0,maxX, maxY);
    }

    /**
     * Clear all objects in this list with coordinates falling outside the X and Y coordinates
     * @param minX      Minimum X coordinate
     * @param minY      Minimum Y coordinate
     * @param maxX      Maximum X coordinate
     * @param maxY      Maximum Y coordinate
     */
    public void removeAllOutOfBounds(int minX, int minY, int maxX, int maxY) {
        synchronized (this) {
            for(T item : this) {
                if(item.getX() < minX || item.getX() > maxX || item.getY() < minY || item.getY() > maxY)
                    this.remove(item);
            }
        }
    }

    public boolean registerPositionedObjectListener(ObjectStateListener<T> listener) {
        return this.listeners.add(listener);
    }

    public boolean deregisterPositionedObjectListener(ObjectStateListener<T> listener) {
        return this.listeners.remove(listener);
    }

    private void notifyObjectAdded(T object) {
        this.listeners.forEach(t -> t.added(object));
    }

    private void notifyObjectRemoved(T object) {
        this.listeners.forEach(t -> t.removed(object));
    }
}
