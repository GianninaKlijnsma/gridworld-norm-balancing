package environment.objectState;

import environment.objects.AgentObject;

import java.awt.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AgentPositionStates extends ObjectPositionStates<AgentObject> {

    private final HashMap<Object, AgentObject> agentPositions = new HashMap<>();

    public boolean addObject(AgentObject agent) {
        if (!this.agentPositions.containsKey(agent.getAgent().getAgentIdentifier())) {
            this.agentPositions.put(agent.getAgent().getAgentIdentifier(), agent);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return this.agentPositions.size();
    }

    public AgentObject get(Object agentIdentifier) {
        return this.agentPositions.get(agentIdentifier);
    }

    public AgentObject removeAgent(Object agentIdentifier) {
        return this.agentPositions.remove(agentIdentifier);
    }

    public List<AgentObject> getReadableObjectStore() {
        return List.copyOf(this.agentPositions.values());
    }

    public Iterator<AgentObject> getIterator() {
        return this.agentPositions.values().iterator();
    }

    @Override
    public AgentObject getObjectAt(Point point) {
        synchronized (this.agentPositions) {
            for (AgentObject o : this.agentPositions.values()) {
                if(o.getPosition().equals(point)) return o;
            }
        }
        return null;
    }

    public boolean containsAgent(Object agentIdentifier) {
        return this.agentPositions.containsKey(agentIdentifier);
    }

    public boolean isFree(Point p) {
        synchronized (this.agentPositions) {
            for (AgentObject t : this.agentPositions.values()) {
                if (t.isPosition(p)) return false;
            }
            return true;
        }
    }

    public AgentObject removeObject(int x, int y) {
        AgentObject removedObject = null;
        synchronized (this.agentPositions) {
            for (Object aid : this.agentPositions.keySet()) {
                if (this.agentPositions.get(aid).isPosition(x, y)) {
                    removedObject = this.agentPositions.remove(aid);
                    break;
                }
            }
        }
        return removedObject;
    }

    public AgentObject getObject(Object agentID) {
        return this.agentPositions.get(agentID);
    }

    public AgentObject getObjectAt(int x, int y) {
        synchronized (this.agentPositions) {
            for (AgentObject agentObject : this.agentPositions.values()) {
                if (agentObject.isPosition(x, y)) return agentObject;
            }
        }
        return null;
    }

    public Collection<AgentObject> getAgentList() {
        return this.agentPositions.values();
    }
}
