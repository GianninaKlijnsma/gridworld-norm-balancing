package environment;

import java.util.Observable;

public class Signal extends Observable {
    private String _name;

    private long _counter = 0;

    public Signal( String name ) {
        _name = name;
    }

    public void emit() {
        emit( null );
    }

    public void emit( Object o ) {
        setChanged();
        notifyObservers( o );
        _counter++;
    }

    public String toString() {
        return _name;
    }

    public String getName() {
        return _name;
    }

    public long getEmitCount() {
        return _counter;
    }
}
