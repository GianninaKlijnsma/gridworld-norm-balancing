package environment;

import environment.event.ObjectStateListener;
import environment.exceptions.ExternalActionFailedException;
import environment.objectState.AgentPositionStates;
import environment.objectState.ObjectPositionStates;
import environment.objects.*;
import environment.view.AgentColor;
import environment.view.Window;


import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Environment {
    // To hold our reference to the window
    private final Window window;

    // max distance of cells visible to each agent
    private int senserange = 5;

    // size of environment
    private Dimension size = new Dimension( 16, 16 );

    /* ---- ALL the stuff in the environment -----*/
    private final AgentPositionStates agents = new AgentPositionStates();
    private final ObjectPositionStates<BombObject> bombs = new ObjectPositionStates<>();
    private final ObjectPositionStates<BombTrap> bombTraps = new ObjectPositionStates<>();
    private final ObjectPositionStates<WallObject> walls = new ObjectPositionStates<>();

    /* ---- SIGNALS -------------------------------*/
    // / emitted on collection of a bomb in the bomb trap
    private transient Signal signalBombTrapped = new Signal( "env bomb trapped" );

    private transient Signal signalSenseRangeChanged = new Signal(
            "env sense range changed" );

    // emitted if environment is resized
    private transient Signal signalSizeChanged = new Signal( "env size changed" );

    // emitted if bomb traps location changed
    private transient Signal signalTrapChanged = new Signal(
            "env trap position changed" );

    private String objType = "default";

    public String getObjType() {
        return this.objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    /**
     * Default constructor
     */
    public Environment()
    {
        super();

        // Create the window
        this.window = new Window(this);
    }

    public Environment(Dimension size) {
        super();

        this.size = size;

        // Create the window
        this.window = new Window(this);
    }

    public Environment(boolean headless) {
        super();

        if(!headless)
            this.window = new Window(this);
        else
            window = null;
    }

    public Environment(Dimension size, boolean headless) {
        super();

        this.size = size;

        if(!headless)
            this.window = new Window(this);
        else
            window = null;
    }

    /* Called from 2APL */

    // Enter the agent into the world
    // Succesful returns true, else the ExternalActionFailedException exception is thrown

    /**
     * Enter the agent in the world.
     * @param agent     2APL Agent to enter the environment
     * @param point     The x and y coordinates where the agent should be spawned
     * @param color     An agent color
     * @return True iff succesful. Otherwise, an exception will be thrown
     * @throws ExternalActionFailedException if agent cannot be added
     */
    public boolean enter(AgentInterface agent, Point point, AgentColor color) throws ExternalActionFailedException
    {
        AgentObject ao;
        synchronized (this.agents) {
            if (this.agents.containsAgent(agent.getAgentIdentifier())) {
                writeToLog("agent already entered");
                throw new ExternalActionFailedException("Agent \"" + agent.getAgentName() + "\" has already entered.");
            }

            // Is this position within the world?
            if (isOutOfBounds(point)) {
                throw new ExternalActionFailedException(String.format(
                        "Position %dx%d out of bounds. Size is %dx%d",
                        point.x, point.y, this.size.width, this.size.height));
            }
            // Is this position free?
            if (!isFree(point)) {
                throw new ExternalActionFailedException(String.format(
                        "Position %dx%d is not free.", point.x, point.y));
            }

            ao = new AgentObject(agent, color, point);
            this.agents.addObject(ao);
        }

        // Give a signal that we want to move
        ao.signalMove.emit();

        writeToLog( "Agent entered: " + agent.getAgentName());

        // Redraw so we can see the agent
        redrawWindow();

        // We came so far, this means success!
        ao.signalMoveSucces.emit();

        return true;
    }

    /**
     * Move the agent one step to the north
     * @param agentIdentifier   AgentID of agent to move
     * @return          Boolean indicating success
     * @throws ExternalActionFailedException if agent cannot be moved in this direction
     */
    public void north(Object agentIdentifier) throws ExternalActionFailedException
    {
        synchronized (this.agents) {
            // Get the correct agent
            AgentObject agent = this.agents.getObject(agentIdentifier);

            // Create new point
            Point p = new Point(agent.getPosition().x, agent.getPosition().y - 1);

            // Set the position for the agent
            if (!setAgentPosition(agent, p))
                throw new ExternalActionFailedException("Moving north failed.");
        }

        // Redraw the window
        redrawWindow();
    }

    /**
     * Move the agent one step to the east
     * @param agentIdentifier   AgentID of agent to move
     * @return          Boolean indicating success
     * @throws ExternalActionFailedException if agent cannot be moved in this direction
     */
    public void east(Object agentIdentifier) throws ExternalActionFailedException
    {
        synchronized (this.agents) {
            // Get the correct agent
            AgentObject agent = this.agents.getObject(agentIdentifier);

            // Create new point
            Point p = new Point(agent.getPosition().x + 1, agent.getPosition().y);

            // Set the position for the agent
            if (!setAgentPosition(agent, p))
                throw new ExternalActionFailedException("Moving east failed.");
        }

        // Redraw the window
        redrawWindow();
    }

    // Move the agent south
    public void south(Object agentIdentifier) throws ExternalActionFailedException
    {
        synchronized (this.agents) {
            // Get the correct agent
            AgentObject agent = this.agents.getObject(agentIdentifier);

            // Create new point
            Point p = new Point(agent.getPosition().x, agent.getPosition().y + 1);

            // Set the position for the agent
            if (!setAgentPosition(agent, p))
                throw new ExternalActionFailedException("Moving south failed.");
        }

        // Redraw the window
        redrawWindow();
    }

    // Move the agent west
    public void west(Object agentIdentifier) throws ExternalActionFailedException
    {
        synchronized (this.agents) {
            // Get the correct agent
            AgentObject agent = this.agents.getObject(agentIdentifier);

            // Create new point
            Point p = new Point(agent.getPosition().x - 1, agent.getPosition().y);

            // Set the position for the agent
            if (!setAgentPosition(agent, p))
                throw new ExternalActionFailedException("Moving west failed.");
        }

        // Redraw the window
        redrawWindow();
    }

    // Pickup a bomb
    public void pickup(Object agentIdentifier ) throws ExternalActionFailedException {

        synchronized (this.agents) {
            // Get the agent
            AgentObject agent = this.agents.get(agentIdentifier);

            // Let everyone know we are going to pick up a bomb
            agent.signalPickupBomb.emit();

            // see if we are not already carrying a bomb
            if (agent.atCapacity()) {
                writeToLog("Pickup bomb failed");
                throw new ExternalActionFailedException("Pickup bomb failed");
            }

            synchronized (this.bombs) {

                // we are not already carying a bomb so get this one
                BombObject bomb = removeBomb(agent.getPosition());
                if (bomb == null) {
                    writeToLog("Pickup bomb failed");
                    throw new ExternalActionFailedException("Pickup bomb failed");
                }

                // Yes
                agent.signalPickupBombSucces.emit();

                // there was a bomb at that position, so set token
                agent.pickup(bomb);
            }
        }

        // show what happened
        redrawWindow();
    }

    // Drop a bomb
    public void drop( Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            // Get the agent
            AgentObject agent = this.agents.get(agentIdentifier);

            // we are going to drop a bomb
            agent.signalDropBomb.emit();

            if (agent.getCarryingBomb() == null) {
                writeToLog("Drop bomb failed");
                throw new ExternalActionFailedException("Drop bomb failed");
            }

            Point pos = agent.getPosition();
            // see if we can drop that bomb here

            synchronized (this.bombs) {
                synchronized (this.bombTraps) {
                    synchronized (this.walls) {
                        if (isBomb(pos) || isStone(pos)) {
                            writeToLog("Drop bomb failed");
                            throw new ExternalActionFailedException("Drop bomb failed");
                        }

                        if (isTrap(pos)) {
                            signalBombTrapped.emit();

                            // Bomb has been cleared. Remove
                            this.bombs.remove(agent.getCarryingBomb());
                        } else {
                            this.bombs.add(agent.getCarryingBomb());
                        }
                    }
                }
            }

            // unset token
            agent.dropBomb();

            agent.signalDropBombSucces.emit();
        }

        // Show it
        redrawWindow();
    }

    /**
     * Sense all agents within the visible area of the agent. Excludes self
     * @param agentIdentifier   ID of agent that wants to sense other agents
     * @return          Hashmap of agentIdentifier's and locations of agents within sense range
     * @throws ExternalActionFailedException
     */
    public HashMap<AgentInterface, Point> senseAgents(Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            AgentObject me = this.agents.get(agentIdentifier);

            HashMap<AgentInterface, Point> sensedAgents = new HashMap<>();

            Iterator<AgentObject> it = this.agents.getIterator();
            while (it.hasNext()) {
                AgentObject ao = it.next();
                if (!ao.equals(me) && this.inSenseRange(me, ao)) {
                    sensedAgents.put(ao.getAgent(), ao.getPosition());
                }
            }

            return sensedAgents;
        }
    }

    /**
     * Allow an agent to sense its position in the environment (always exact position)
     * @param agentIdentifier   ID of agent that wants to sense its position
     * @return          Point object containing X and Y coordinates of position
     * @throws ExternalActionFailedException
     */
    public Point sensePosition(Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            AgentObject agent = this.agents.getObject(agentIdentifier);
            return (Point) agent.getPosition().clone();
        }
    }

    /**
     * Sense bomb traps within visible area of an agent
     * @param agentIdentifier   Agent that performs the sense action
     * @return          List of points containing bomb traps
     * @throws ExternalActionFailedException
     */
    public List<Point> senseTraps(Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            synchronized (this.bombTraps) {
                AgentObject me = this.agents.get(agentIdentifier);
                return this.bombTraps.stream().filter(o -> inSenseRange(me, o)).map(BombTrap::getPosition).collect(Collectors.toList());
            }
        }
    }


    List<Point> getTrapPositions()
    {
        synchronized (this.bombTraps) {
            return this.bombTraps.stream().map(BombTrap::getPosition).collect(Collectors.toList());
        }
    }

    /**
     * Sense all bombs in the visible area of an agent
     * @param agentIdentifier   AgentID of agent that performs sense act
     * @return          List of coordinates of bombs within visible area of agent
     * @throws ExternalActionFailedException
     */
    public List<Point> senseBombs(Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            synchronized (this.bombTraps) {
                AgentObject me = this.agents.get(agentIdentifier);
                return this.bombs.stream().filter(o -> inSenseRange(me, o)).map(PositionedObject::getPosition).collect(Collectors.toList());
            }
        }
    }

    /**
     * Find the coordinates of all bombs in the environment
     * @return  List of coordinates of bombs in the environment
     */
    List<Point> getAllBombLocations ()
    {
        synchronized (this.bombs) {
            return this.bombs.stream().map(BombObject::getPosition).collect(Collectors.toList());
        }
    }

    /**
     * Sense all wall objects within the visible area of the agent
     * @param agentIdentifier   Agent that does the sensing
     * @return          List of coordinates of stones within visible area of agent
     * @throws ExternalActionFailedException
     */
    public List<Point> senseStones(Object agentIdentifier) throws ExternalActionFailedException {
        synchronized (this.agents) {
            synchronized (this.walls) {
                AgentObject me = this.agents.get(agentIdentifier);
                return this.walls.stream().filter(w -> inSenseRange(me, w)).map(WallObject::getPosition).collect(Collectors.toList());
            }
        }
    }

    /**
     * Get all walls in the environment
     * @return List of coordinates of walls
     */
    List<Point> getAllWalls()
    {
        synchronized (this.walls) {
            return this.walls.stream().map(WallObject::getPosition).collect(Collectors.toList());
        }
    }

    /* Standard functions --------------------------------------*/

    private void notifyEvent(String parm1, Point point)
    {
        // TODO events can be processed here, if a better view is required
    }

    // Remove the agent from the environment
    public void removeAgent(Object agentIdentifier)
    {
        synchronized (this.agents) {
            AgentObject a = this.agents.removeAgent(agentIdentifier);

            writeToLog("Agent removed: " + a.getAgent().getAgentName());
        }

        synchronized (this) {
            notifyAll();
        }
    }

    /* END Standard functions --------------------------------------*/


    /* Helper functions --------------------------------------*/

    // Get the size of the grid world
    public Dimension getWorldSize(String agent)
    {
        return this.size;
    }

    private static String getMainModule(String sAgent)
    {
        int dotPos;
        if ((dotPos = sAgent.indexOf('.')) == -1)
            return sAgent;
        else
            return sAgent.substring(0, dotPos);
    }

    // Get the environment width
    public int getWidth() {	return size.width; }

    // Get the environment height
    public int getHeight() { return size.height; }

    // Return the agents
    Collection<AgentObject> getGridWorldAgents()
    {
        synchronized (this.agents) {
            return new ArrayList<>(this.agents.getAgentList());
        }
    }

    // Get senserange
    public int getSenseRange()
    {
        return senserange;
    }

    // Redrawing the window is a nightmare, this does some redraw stuff
    private void validatewindow()
    {
        if(this.window != null) {
            SwingUtilities.invokeLater(window::doLayout);
        }
    }

    // Move the agent
    private boolean setAgentPosition( AgentObject agent, Point point)
    {
        synchronized (this.agents) {
            agent.signalMove.emit();

            if (isOutOfBounds(point)) {
                return false;
            }

            // suspend thread if some other agent is blocking our entrance

            // Is the position free?
            if (!agent.getPosition().equals(point) && !isFree(point)) {
                return false;
            }

            agent.signalMoveSucces.emit();

            // there may be other threads blocked because this agent was in the way,
            // notify
            // them of the changed state of environment
            synchronized (this) {
                notifyAll();
            }

            // set the agent position
            agent.setPosition(point);
            return true;
        }
    }

    // check if point is within environment boundaries
    // return false is p is within bounds
    private boolean isOutOfBounds(Point point)
    {
        return (point.x >= size.getWidth()) || (point.x < 0) || (point.y >= size.getHeight()) || (point.y < 0);
    }

    // Is the position free?
    private boolean isFree(Point point)
    {
        synchronized (this.walls) {
            synchronized (this.agents) {
                return this.walls.isFree(point) && this.agents.isFree(point);
            }
        }
    }

    // Check for agent at coordinate. \return Null if there is no agent at the
    // specified coordinate. Otherwise return a reference to the agent there.
    boolean isAgent(Point point)
    {
        synchronized (this.agents) {
            return !this.agents.isFree(point);
        }
    }

    AgentObject getAgentAtPoint(Point point) {
        synchronized (this.agents) {
            return this.agents.getObjectAt(point);
        }
    }

    // Is there a stone at this point
    boolean isStone(Point point)
    {
        synchronized (this.walls) {
            return !this.walls.isFree(point);
        }
    }

    public WallObject getStoneAtPoint(Point point) {
        synchronized (this.walls) {
            return this.walls.getObjectAt(point);
        }
    }

    // see if there is a trap at the specified coordinate
    boolean isTrap(Point point) {
        synchronized (this.bombTraps) {
            return !this.bombTraps.isFree(point);
        }
    }

    public BombTrap getTrapAtPoint(Point point) {
        synchronized (this.bombTraps) {
            return this.bombTraps.getObjectAt(point);
        }
    }

    private synchronized boolean inSenseRange(PositionedObject o1, PositionedObject o2) {
        return o1.getPosition().distance(o2.getPosition()) <= this.senserange;
    }

    boolean isBomb(Point point)
    {
        synchronized (this.bombs) {
            return !this.bombs.isFree(point);
        }
    }

    public BombObject getBombAtPoint(Point point) {
        synchronized (this.bombs) {
            return this.bombs.getObjectAt(point);
        }
    }

    // TODO probably all these signals and notifies can now be removed
    BombObject removeBomb(Point point) {
        synchronized (this.bombs) {
            BombObject bomb = this.bombs.removeObjectAt(point);
            if (bomb != null) {
                notifyEvent("bombRemovedAt", point);
                synchronized (this) {
                    notifyAll();
                }

            }
            return bomb;
        }
    }

    // removeAgent stone at position
    boolean removeStone(Point point)
    {
        synchronized (this.walls) {
            WallObject stone = this.walls.removeObjectAt(point);
            if (stone != null) {
                notifyEvent("wallRemovedAt", point);
                synchronized (this) {
                    notifyAll();
                }
            }
            return stone != null;
        }
    }

    // removeAgent trap at position
    boolean removeTrap(Point point)
    {
        synchronized (this.bombTraps) {
            BombTrap bombTrap = this.bombTraps.removeObjectAt(point);
            if (bombTrap != null) {
                notifyEvent("trapRemovedAt", point);
                synchronized (this) {
                    notifyAll();
                }
            }
            return bombTrap != null;
        }
    }

    // Add a stone at the given position
    boolean addStone(Point point) throws IndexOutOfBoundsException
    {
        // valid coordinate
        if( isOutOfBounds( point ) )
            throw new IndexOutOfBoundsException( String.format(
                    "setStone out of range: %dx%d not within %dx%d",
                    point.x, point.y, size.width, size.height));

        // is position clear of other stuff
        // Changed SA:
        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    if (isBomb(point) || isStone(point) || isTrap(point))
                        return false;

                    this.walls.add(new WallObject(point));
                }
            }
        }

        notifyEvent("wallAt", point);

        return true;
    }

    // Add a bomb to the environment
    boolean addBomb(Point point) throws IndexOutOfBoundsException
    {
        if( isOutOfBounds( point ) )
            throw new IndexOutOfBoundsException( String.format(
                    "addBomb out of range: %dx%d not within %dx%d",
                    point.x, point.y, size.width, size.height));

        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    // is position clear of other stuff
                    if (isBomb(point) || isStone(point) || isTrap(point))
                        return false;

                    // all clear, accept bomb
                    this.bombs.add(new BombObject(point));
                }
            }
        }

        notifyEvent("bombAt", point);

        return true;
    }

    // Add a trap at the given position
    boolean addTrap(Point point) throws IndexOutOfBoundsException {
        // valid coordinate
        if( isOutOfBounds( point ) )
            throw new IndexOutOfBoundsException( String.format(
                    "setTrap out of range: %dx%d not within %dx%d",
                    point.x, point.y, size.width, size.height));

        // is position clear of other stuff
        // Changed SA:
        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    if (isBomb(point) || isStone(point) || isTrap(point))
                        return false;

                    this.bombTraps.add(new BombTrap(point ));
                }
            }
        }

        notifyEvent("trapAt", point);

        return true;
    }

    // Print a message to the console
    static public void writeToLog(String message) {
        //System.out.println("gridworld: " + message);
    }

    // Set the senserange
    public void setSenseRange( int senserange )
    {
        this.senserange = senserange;
        signalSenseRangeChanged.emit();
    }

    // helper function, calls setSize(Dimension)
    public void setSize( int width, int height )
    {
        setSize( new Dimension( width, height ) );
    }

    // resize world
    public void setSize( Dimension size )
    {
        this.size = size;
        signalSizeChanged.emit();
        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    this.bombs.removeAllOutOfBounds(0, 0, getWidth(), getHeight());
                    this.bombTraps.removeAllOutOfBounds(0, 0, getWidth(), getHeight());
                    this.walls.removeAllOutOfBounds(0, 0, getWidth(), getHeight());
                }
            }
        }
    }

    // Remove everything
    public void clear()
    {
        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    this.walls.clear();
                    this.bombs.clear();
                    this.bombTraps.clear();
                }
            }
        }
    }

    // Save the environment
    public void save( OutputStream destination ) throws IOException
    {
        ObjectOutputStream stream = new ObjectOutputStream( destination );
        stream.writeObject(this.size);
        stream.writeInt(this.senserange);

        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    stream.writeObject(this.walls);
                    stream.writeObject(this.bombs);
                    stream.writeObject(this.bombTraps);
                }
            }
        }
        stream.flush();
    }

    // Load the environment
    public void load( InputStream source ) throws IOException, ClassNotFoundException
    {
        ObjectInputStream stream = new ObjectInputStream( source );
        Dimension size = (Dimension) stream.readObject();

        int senserange = stream.readInt();

        ObjectPositionStates<WallObject> stones = (ObjectPositionStates<WallObject>) stream.readObject();
        ObjectPositionStates<BombObject> bombs = (ObjectPositionStates<BombObject>) stream.readObject();
        ObjectPositionStates<BombTrap> traps = (ObjectPositionStates<BombTrap>) stream.readObject();

        // delay assignments until complete load is succesfull
        this.size = size;
        this.senserange = senserange;

        signalSizeChanged.emit();
        signalTrapChanged.emit();
        signalSenseRangeChanged.emit();

        clear();
        synchronized (this.bombs) {
            synchronized (this.walls) {
                synchronized (this.bombTraps) {
                    this.walls.clear();
                    this.bombs.clear();
                    this.bombTraps.clear();
                    this.walls.addAll(stones);
                    this.bombs.addAll(bombs);
                    this.bombTraps.addAll(traps);
                }
            }
        }

        this.redrawWindow();
    }

    /* END Helper functions --------------------------------------*/

    boolean addAgentStateChangeListener(ObjectStateListener<AgentObject> listener) {
        return false;
    }

    boolean addBombStateChangeListener(ObjectStateListener<BombObject> listener) {
        return this.bombs.registerPositionedObjectListener(listener);
    }

    boolean addTrapStateChangeListener(ObjectStateListener<BombTrap> listener) {
        return this.bombTraps.registerPositionedObjectListener(listener);
    }

    boolean addWallStateChangeListener(ObjectStateListener<WallObject> listener) {
        return this.walls.registerPositionedObjectListener(listener);
    }

    boolean removeAgentStateChangeListener(ObjectStateListener<AgentObject> listener) {
        return false;
    }

    boolean removeBombStateChangeListener(ObjectStateListener<BombObject> listener) {
        return this.bombs.deregisterPositionedObjectListener(listener);
    }

    boolean removeTrapStateChangeListener(ObjectStateListener<BombTrap> listener) {
        return this.bombTraps.deregisterPositionedObjectListener(listener);
    }

    boolean removeWallStateChangeListener(ObjectStateListener<WallObject> listener) {
        return this.walls.deregisterPositionedObjectListener(listener);
    }

    public Point getFirstFree() {
        return getFirstFree(0, 0);
    }

    public Point getFirstFree(Point after) {
        return getFirstFree(after.x, after.y);
    }

    public Point getFirstFree(int minColumn, int minRow) {
        for(int i = minColumn; i < getWidth(); i++) {
            for(int j = minRow; j < getHeight(); j++) {
                if(isFree(new Point(i,j))) return new Point(i,j);
            }
        }

        return null;
    }

    /* END Liseteners ------------------------------------------*/


    /* Overrides for ObsVector ---------------------------------*/

    public void onAdd( int index, Object o ) {}

    public void onRemove( int index, Object o )
    {
        // TODO fix this shit
//        ((Agent) o).deleteObservers();
    }

    private void redrawWindow() {
        if(this.window != null) {
            validatewindow();
            this.window.repaint();
        }
    }

    /* END Overrides for ObsVector ---------------------------------*/
}
