package environment.view;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageProducer;
import java.net.URL;


public enum AgentColor {

    ARMY(Color.GREEN),
    BLUE(Color.BLUE),
    GRAY(Color.GRAY),
    ORANGE(Color.ORANGE),
    PINK(Color.PINK),
    PURPLE(Color.MAGENTA),
    RED(Color.RED),
    TEAL(Color.CYAN);

    private String name;
    private Color color;
    private Image imageAgent;
    private Image imageHolding;

    private final Component c = new JPanel();

    AgentColor(Color color) {
        this.name = this.toString().toLowerCase();
        this.color = color;
        URL imageAgentUrl = getClass().getClassLoader().getResource(String.format("images/agents/agent_%s.gif", this.name));
        URL imageHoldingUrl = getClass().getClassLoader().getResource(String.format("images/agents/holding_%s.gif", this.name));
        try {
            this.imageAgent = c.createImage((ImageProducer) imageAgentUrl.getContent());
            this.imageHolding = c.createImage((ImageProducer) imageHoldingUrl.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Color getColor() {
        return this.color;
    }

    public String getLabel() {
        return this.name;
    }

    public Image getImageAgent() {
        return this.imageAgent;
    }

    public Image getImageHolding() {
        return this.imageHolding;
    }
}
