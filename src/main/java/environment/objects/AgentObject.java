package environment.objects;

import environment.AgentInterface;
import environment.Signal;
import environment.view.AgentColor;

import java.awt.*;

public class AgentObject extends PositionedObject {

    private AgentColor color;

    private BombObject carryingBomb = null;

    public AgentObject(AgentInterface agent, AgentColor colour, Point point) {
        super(point);
        this.agent = agent;
        this.color = colour;
    }

    /**
     * The agent on the platform this object encodes
     */
    public AgentInterface agent;

    public AgentInterface getAgent() {
        return agent;
    }

    public void setAgent(AgentInterface agent) {
        this.agent = agent;
    }

    public AgentColor getColor() {
        return color;
    }

    public void setColor(AgentColor colour) {
        this.color = colour;
    }

    // / emitted if agent attemps movement (succesful or not)
    public transient Signal signalMove = new Signal( "agent attempts move" );

    // / emitted if agent attemps to pickup a bomb (succesful or not)
    public transient Signal signalPickupBomb = new Signal(
            "agent attempts pickup" );

    // / emitted if agent attemps to drop a bomb (succesful or not)
    public transient Signal signalDropBomb = new Signal( "agent attempts drop" );

    // / emitted if agent succesfully moves
    public transient Signal signalMoveSucces = new Signal(
            "agent succesful move" );

    // / emitted if agent (succesfully) picks up a bomb
    public transient Signal signalPickupBombSucces = new Signal(
            "agent succesful pickup" );

    // / emitted if agent (succesfully) drops a bomb
    public transient Signal signalDropBombSucces = new Signal(
            "agent sucessful drop" );

    public void setPosition(Point point) {
        this.position = point;
    }

    public boolean dropBomb() {
        if(this.carryingBomb == null) return false;

        this.carryingBomb.drop(this);
        this.carryingBomb = null;
        return true;
    }

    public void pickup(BombObject bomb) {
        if(bomb.getPosition().equals(this.position)) {
            this.carryingBomb = bomb;
            bomb.pickedupBy(this);
        }
    }

    public BombObject getCarryingBomb() {
        return this.carryingBomb;
    }

    public boolean atCapacity() {
        return this.carryingBomb != null;
    }
}
