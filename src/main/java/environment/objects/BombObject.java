package environment.objects;

import java.awt.*;

public class BombObject extends PositionedObject {

    private Object carriedBy = null;

    public BombObject(Point point) {
        super(point);
    }

    void drop(AgentObject agent) {
        if(!agent.getAgent().getAgentIdentifier().equals(this.carriedBy))
            throw new IllegalCallerException("An agent that is not carrying this bomb is trying to drop it");
        this.position = agent.getPosition();
        this.carriedBy = null;
    }

    // Intentionally package-private
    void pickedupBy(AgentObject object) {
        if(this.moving()) throw new IllegalStateException("Bomb is already carried by an agent");
        if(!object.getPosition().equals(this.position)) throw new IllegalCallerException("Agent trying to pick up bomb, but is not in same grid cell");

        this.carriedBy = object.getAgent().getAgentIdentifier();
    }

    public boolean moving() {
        return this.carriedBy != null;
    }

}
