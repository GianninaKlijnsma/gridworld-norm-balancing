package environment.objects;

import java.awt.*;
import java.io.Serializable;

/**
 * Encodes an object occupying exactly one cell in the grid
 */
public class PositionedObject implements Serializable {

    Point position;

    /**
     * Create a new Positioned Object
     * @param x     Starting X coordinate
     * @param y     Starting Y coordinate
     */
    public PositionedObject(int x, int y) {
        this.position = new Point(x, y);
    }

    /**
     * Create a new Positioned Object
     * @param point     Starting coordinates
     */
    public PositionedObject(Point point) { this.position = point; }

    public int getX() {
        return this.position.x;
    }

    public int getY() {
        return this.position.y;
    }

    public Point getPosition() {
        return this.position;
    }

    /**
     * Check whether this object occupies a specific cell
     * @param x     X coordinate of the cell to check
     * @param y     Y coordinate of the cell to check
     * @return      True iff this object is at in specified position
     */
    public boolean isPosition(int x, int y) {
        return this.position.equals(new Point(x, y));
    }

    /**
     * Check whether this object is in the same position as another PositionedObject
     * @param o     The other PositionedObject
     * @return      True iff this object is in the same position as the other PositionedObject
     */
    public boolean isPosition(PositionedObject o) {
        return this.position.equals(o.position);
    }

    /**
     * Check whether this object occupies a specific cell
     * @param point     Coordinates of the cell to check
     * @return      True iff this object is at in specified position
     */
    public boolean isPosition(Point point) {
        return this.position.equals(point);
    }
}
