# Grid World: Norm and Goal Balancing
This application is an extension of the original Grid World : Harry and Sally example, which can be found at https://bitbucket.org/goldenagents/grid-world/ . The Grid World application is a demo of how 2APL agents can interact in a virtual environment. The environment in question is a Grid World, a cell in which can contain an agent, a bomb, a bomb trap or a wall. The agents should scout the environment for bombs and, when found, carry bombs to a bomb trap to remove it from the environment, all the while moving around obstacles such as walls and other agents.

In this demo application, two (types of) agents are instantiated: Harry and Sally. Sally walks across the environment looking for the locations of bombs and bomb traps, informing Harry of those locations when found. When receiving a newly scouted location, Harry updates its belief base and sets out to move to the closest bomb location, pick up the bomb there and carry it to a bomb trap location, where it drops the bomb to remove it from the environment.

Furthermore, four types of norms are defined. Each type of norm prohibits an agent from moving into a certain direction (north, west, east, south) if it is at a certain location. Furthermore, this application uses the agent model in https://bitbucket.org/GianninaKlijnsma/net2apl-norm-balancing/ so that the agents can balance their norms and goals. The model makes use of a list of properties that the agent can use to calculate the utility of a plan. A property gives a value to a certain aspect of a plan, which is between the interval of `[-1,1]`. The higher the value, the more the property is satisfied. Furthermore, the agent defines weights over each property, specifying how important that property is for the agent. These are also in the interval of `[-1, 1]`, and the higher the weight, the more the agent cares about this property. The utility of a plan is then calculated by summing the weighted property values together. If the resulting utility is negative, the plan and its goal are dropped. Otherwise, the plan will be executed.

## Parameters
The environment is instantiated as a 50x50 cells grid with one Harry, one Sally, who wait 120ms after every step they take by default.
The `Main` class allows changing these parameters:
1. `N_HARRIES` and `N_SALLIES` specify how many Harry and Sally agents resp. are instantiated in the environment
2. `ENV_WIDTH` and `ENV_HEIGHT` allow changing the initial Grid World width and height respectively
3. `INSTANTIATE_ENVIRONMENT_HEADLESS` allows running the simulation in headless mode by setting this variable to false. This will not create the view 
4. `AFTER_MOVE_DELAY` specifies the number of milliseconds agents wait after each succesful step. This allows you to see the agents move. This delay can be changed (or set to `0`), which may be useful if more agents are instantiated
5. `PROPERTY_WEIGHT_HARRY` and `PROPERTY_WEIGHT_SALLY` can be used to change the weights that Harry and Sally give to each property. These variables are given for each property, and must be within the interval of `[-1, 1]`

Three example environments are provided in the root directory of this repository (which will automatically resize the Grid World to 50x50). These can be loaded through the options menu after the simulation has started. 

# Installation
Requires Java 11 or higher
## IDE
The project can be imported as a Maven project in any supporting IDE. The 2APL library needs to be installed using Maven beforehand, however, as it is not in the Maven repositories. Using a UNIX terminal with Java, Git, and Maven installed, run the commands in the first box in the following section (installing the `net2apl` repository).

After 2APL is installed, import the project as a Maven project in your supporting IDE, run the Maven installer in said IDE and run `Main.java`.

## Pure maven
First make sure 2APL is installed as a Maven repository on your machine:

```bash
$ git clone https://bitbucket.org/GianninaKlijnsma/net2apl-norm-balancing.git
$ cd net2apl-norm-balancing
$ mvn install
```

Now the GridWorld can be installed, by cloning this repository and installing with Maven using the string of commands below, or load the project in your favorite IDE with Maven support.

```bash
$ cd ../
$ git clone https://@bitbucket.org/GianninaKlijnsma/gridworld-norm-balancing.git
$ cd gridworld-norm-balancing
$ mvn install
```

## 
The project can be run without Maven, but 2APL needs to be added as a source of your project. Since 2APL is setup as a Maven project as well, the easiest way is to clone both repositories and create a simlink in the appropriate location. The following commands assume you will clone both projects in your home directory:

```bash
cd ~
git clone https://@bitbucket.org/GianninaKlijnsma/gridworld-norm-balancing.git
https://bitbucket.org/GianninaKlijnsma/net2apl-norm-balancing.git
cd ~/gridworld-norm-balancing/src/main/java/
ln -sv ~/net2apl-norm-balancing/src/org
```

Now compile `Main.java` in the directory the previous commands left you in, or import the entire
project as a normal Java project in your (non-Maven supporting) IDE.
